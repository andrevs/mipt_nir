package com.sbt.mipt.gems.proxy;

import org.junit.Test;

import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Test for {@link ReadOnlyObjectTransformer}
 *
 * @author Vasyukevich Andrey
 */
public class ReadOnlyObjectTransformerTest {
    @Test
    public void testProxy() {
        Dto o = new Dto("a", new InnerDto(Arrays.asList(new MyImmutableInt(1), new MyImmutableInt(2))));

        DtoI proxy = ReadOnlyObjectTransformer.makeReadOnly(o);

        assertEquals(o.getA(), proxy.getA());
        assertEquals(o.getB().getC(), proxy.getB().getC());

        assertNotNull(catchException(() -> proxy.setA("b")));
        assertNotNull(catchException(() -> proxy.getB().setC(Collections.singletonList(new MyImmutableInt(3)))));
    }

    @Test
    public void testProxyForReadOnlyProxy() {
        Dto o = new Dto("a", new InnerDto(Arrays.asList(new MyImmutableInt(1), new MyImmutableInt(2))));

        DtoI proxy = ReadOnlyObjectTransformer.makeReadOnly(o);
        DtoI proxyProxy = ReadOnlyObjectTransformer.makeReadOnly(proxy);

        assertTrue(proxy == proxyProxy);
    }

    @Test
    public void testProxyForLists() {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4);
        List<Integer> numbersProxy = ReadOnlyObjectTransformer.makeReadOnly(numbers);
        List<Integer> numbersProxyCopy = new ArrayList<>(numbersProxy);

        assertEquals(Integer.class, numbersProxyCopy.get(0).getClass());

        List<List<Integer>> list = Collections.singletonList(numbers);
        List<List<Integer>> listProxy = ReadOnlyObjectTransformer.makeReadOnly(list);
        List<List<Integer>> listProxyCopy = new ArrayList<>(listProxy);

        assertTrue(Proxy.isProxyClass(listProxyCopy.get(0).getClass()));
        assertEquals(Integer.class, listProxyCopy.get(0).get(0).getClass());
    }

    private Exception catchException(Runnable runnable) {
        try {
            runnable.run();
            return null;
        } catch (Exception e) {
            return e;
        }
    }

    public static interface DtoI {
        String getA();

        void setA(String a);

        InnerDtoI getB();

        void setB(InnerDtoI b);
    }

    public static interface InnerDtoI {
        List<? extends MyImmutableIntI> getC();

        void setC(List<? extends MyImmutableIntI> c);
    }

    public static interface MyImmutableIntI {
        int getNumber();
    }

    public static class Dto implements DtoI {
        private String a;
        private InnerDtoI b;

        public Dto(String a, InnerDtoI b) {
            this.a = a;
            this.b = b;
        }

        public String getA() {
            return a;
        }

        public void setA(String a) {
            this.a = a;
        }

        @Override
        public InnerDtoI getB() {
            return b;
        }

        @Override
        public void setB(InnerDtoI b) {
            this.b = b;
        }
    }

    public static class InnerDto implements InnerDtoI {
        private List<? extends MyImmutableIntI> c;

        public InnerDto(List<? extends MyImmutableIntI> c) {
            this.c = c;
        }

        @Override
        public List<? extends MyImmutableIntI> getC() {
            return c;
        }

        @Override
        public void setC(List<? extends MyImmutableIntI> c) {
            this.c = c;
        }
    }

    public static class MyImmutableInt implements MyImmutableIntI {
        int number;

        public MyImmutableInt(int number) {
            this.number = number;
        }

        @Override
        public int getNumber() {
            return number;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof MyImmutableIntI) {
                MyImmutableIntI other = (MyImmutableIntI) obj;
                return (number == other.getNumber());
            }
            return false;
        }
    }
}
