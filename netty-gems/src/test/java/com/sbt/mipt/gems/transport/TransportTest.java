package com.sbt.mipt.gems.transport;

import com.sbt.mipt.gems.transport.client.Client;
import com.sbt.mipt.gems.transport.client.InetClient;
import com.sbt.mipt.gems.transport.client.LocalClient;
import com.sbt.mipt.gems.transport.server.InetServer;
import com.sbt.mipt.gems.transport.server.LocalServer;
import com.sbt.mipt.gems.transport.server.Server;
import com.sbt.mipt.gems.transport.server.ServerCallback;
import io.netty.channel.local.LocalAddress;
import io.netty.channel.nio.NioEventLoopGroup;
import org.junit.Test;

import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

/**
 * Test for {@link Server} and {@link Client} interaction.
 *
 * @author Vasyukevich Andrey
 */
public class TransportTest {
    @Test(timeout = 5000)
    public void localTest() throws InterruptedException {
        SocketAddress serverAddress = new LocalAddress("id");

        Server server = new LocalServer(serverAddress, new EchoServerCallback());
        Thread serverThread = new Thread(server);

        serverThread.start();

        NioEventLoopGroup clientEventLoopGroup = new NioEventLoopGroup();
        Client client1 = new LocalClient(serverAddress, clientEventLoopGroup);
        Client client2 = new LocalClient(serverAddress, clientEventLoopGroup);

        clientServerInteractionTest(client1);
        clientServerInteractionTest(client2);

        client1.close();
        client2.close();
        clientEventLoopGroup.shutdownGracefully();

        server.close();
        
        serverThread.join();
    }

    @Test(timeout = 5000)
    public void inetTest() throws InterruptedException {
        SocketAddress serverAddress = new InetSocketAddress("localhost", 8008);

        Server server = new InetServer(serverAddress, new EchoServerCallback());
        Thread serverThread = new Thread(server);

        serverThread.start();

        NioEventLoopGroup clientEventLoopGroup = new NioEventLoopGroup();
        Client client1 = new InetClient(serverAddress, clientEventLoopGroup);
        Client client2 = new InetClient(serverAddress, clientEventLoopGroup);

        clientServerInteractionTest(client1);
        clientServerInteractionTest(client2);

        client1.close();
        client2.close();
        clientEventLoopGroup.shutdownGracefully();

        server.close();

        serverThread.join();
    }

    private void clientServerInteractionTest(Client client) {
        String message = "abc";

        UUID requestId = client.send(message);

        String response = client.getResponse(requestId);

        assertEquals(message, response);
    }

    public static class EchoServerCallback implements ServerCallback<String, String> {
        @Override
        public String call(String object) {
            return object;
        }
    }
}
