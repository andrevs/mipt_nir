package com.sbt.mipt.gems.task;

import com.sbt.mipt.gems.message.Gem;
import org.junit.Test;

import java.io.*;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * Test for {@link WorkerStatelessTask}
 *
 * @author Vasyukevich Andrey
 */
public class WorkerStatelessTaskTest {
    @Test
    public void serializationTest() throws IOException, ClassNotFoundException {
        TestTask testTask = new TestTask();

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);

        objectOutputStream.writeObject(testTask);

        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
        ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);

        TestTask deserializedTestTask = (TestTask) objectInputStream.readObject();

        assertFalse(testTask == deserializedTestTask);

        assertEquals(testTask.getId(), deserializedTestTask.getId());
    }

    public static class TestTask extends WorkerStatelessTask<String> {
        @Override
        public String execute(Gem gem) {
            return null;
        }

        @Override
        public List<Gem> split(Gem gem, int nWorkers) {
            return null;
        }

        @Override
        public String join(Gem gem, List<String> results) {
            return null;
        }
    }
}
