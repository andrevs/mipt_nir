package com.sbt.mipt.gems.proxy;

/**
 * Objects which implement this interface assumed immutable and returned as is (without read-only proxy wrapper).
 *
 * @author Vasyukevich Andrey
 */
public interface Immutable {
}
