package com.sbt.mipt.gems.transport;

import java.io.Serializable;
import java.util.UUID;

/**
 * Transport message.
 *
 * @author Vasyukevich Andrey
 */
public class Message<T> implements Serializable {
    private final UUID id;
    private final T object;

    private Message(UUID id, T object) {
        this.id = id;
        this.object = object;
    }

    public Message(T object) {
        this(UUID.randomUUID(), object);
    }

    public <R> Message<R> newLinkedMessage(R object) {
        return new Message<>(id, object);
    }

    public UUID getId() {
        return id;
    }

    public T getObject() {
        return object;
    }

    @Override
    public String toString() {
        return "Message(id=" + id + ", object=" + trim(object.toString(), 240) + ")";
    }

    private String trim(String s, int maxLength) {
        return (s.length() < maxLength ? s : s.substring(0, maxLength)) + "...";
    }
}
