package com.sbt.mipt.gems.message;

/**
 * Gem access mode.
 *
 * @author Vasyukevich Andrey
 */
public enum GemAccessMode {
    DEFAULT,
    READ_ONLY
}
