package com.sbt.mipt.gems.coordinator;

/**
 * Connection type for message passing between workers
 *
 * @author Vasyukevich Andrey
 */
public enum ConnectionType {
    INET,
    LOCAL
}
