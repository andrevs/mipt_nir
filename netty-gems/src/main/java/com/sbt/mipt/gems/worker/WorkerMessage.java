package com.sbt.mipt.gems.worker;

import com.sbt.mipt.gems.message.Gem;
import com.sbt.mipt.gems.task.WorkerStatelessTask;

import java.io.Serializable;

/**
 * Message for transferring subtask to workers.
 *
 * @author Vasyukevich Andrey
 */
public class WorkerMessage<R> implements Serializable {
    private final WorkerStatelessTask<R> workerStatelessTask;
    private final Gem gem;

    public WorkerMessage(WorkerStatelessTask<R> workerStatelessTask, Gem gem) {
        this.workerStatelessTask = workerStatelessTask;
        this.gem = gem;
    }

    public WorkerStatelessTask<R> getWorkerStatelessTask() {
        return workerStatelessTask;
    }

    public Gem getGem() {
        return gem;
    }

    @Override
    public String toString() {
        return "WorkerMessage(workerStatelessTask=" + workerStatelessTask + ", gem=" + gem + ")";
    }
}
