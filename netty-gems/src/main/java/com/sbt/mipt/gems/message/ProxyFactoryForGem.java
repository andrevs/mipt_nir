package com.sbt.mipt.gems.message;

import com.sbt.mipt.gems.proxy.ReadOnlyObjectTransformer;

/**
 * Generates proxy for objects in gems.
 *
 * @author Vasyukevich Andrey
 */
public class ProxyFactoryForGem {
    public static <T> T createProxy(T object, GemAccessMode gemAccessMode) {
        switch (gemAccessMode) {
            case DEFAULT:
                return object;
            case READ_ONLY:
                return ReadOnlyObjectTransformer.makeReadOnly(object);
        }

        return null;
    }
}
