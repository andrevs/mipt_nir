package com.sbt.mipt.gems.coordinator;

import com.sbt.mipt.gems.message.Gem;
import com.sbt.mipt.gems.task.WorkerStatelessTask;
import com.sbt.mipt.gems.transport.client.Client;
import com.sbt.mipt.gems.transport.server.Server;
import com.sbt.mipt.gems.worker.WorkerMessage;
import com.sbt.mipt.gems.worker.WorkerMessageServerCallback;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.util.concurrent.Future;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.SocketAddress;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Class for solving tasks using GEM model. It creates workers and uses them to solve submitted tasks.
 *
 * @author Vasyukevich Andrey
 */
public abstract class Coordinator implements AutoCloseable {
    private final static Logger LOGGER = LoggerFactory.getLogger(Coordinator.class);

    private final int nWorkers;
    private final List<? extends SocketAddress> workerAddresses;
    private final List<WorkerMessageServerCallback> workerServerCallbacks;
    private final List<? extends Server> workerServers;
    private final List<Thread> workerThreads;
    private final List<? extends EventLoopGroup> workerServerCallbackClientEventGroups;
    private final EventLoopGroup workerClientsEventGroup;
    private final List<? extends Client> workerClients;

    private final Random random = new Random();

    Coordinator(int nWorkers, Integer maxObjectSize) {
        this.nWorkers = nWorkers;
        this.workerAddresses = createWorkerAddresses();
        this.workerServerCallbacks = createWorkerServerCallbacks();
        this.workerServers = createWorkerServers(workerAddresses, workerServerCallbacks, maxObjectSize);
        this.workerThreads = workerServers.stream().map(Thread::new).collect(Collectors.toList());

        workerThreads.forEach(Thread::start);

        this.workerServerCallbackClientEventGroups = Stream.generate(NioEventLoopGroup::new).limit(nWorkers).collect(Collectors.toList());
        this.workerClientsEventGroup = new NioEventLoopGroup();

        createWorkerClientsForWorkerServerCallbacks(workerAddresses, workerServerCallbacks, maxObjectSize, workerServerCallbackClientEventGroups);

        this.workerClients = createWorkerClients(workerAddresses, maxObjectSize, workerClientsEventGroup);

    }

    public int getnWorkers() {
        return nWorkers;
    }

    protected abstract List<? extends SocketAddress> createWorkerAddresses();

    protected List<WorkerMessageServerCallback> createWorkerServerCallbacks() {
        List<WorkerMessageServerCallback> workerServerCallbacks = new ArrayList<>();

        Map<? extends SocketAddress, Set<String>> executedTasksMap = workerAddresses.stream()
                .collect(Collectors.toMap(addr -> addr, addr -> ConcurrentHashMap.newKeySet()));

        for (int i = 0; i < nWorkers; ++i) {
            SocketAddress workerAddress = workerAddresses.get(i);

            workerServerCallbacks.add(new WorkerMessageServerCallback(workerAddress, executedTasksMap));
        }

        return workerServerCallbacks;
    }

    protected abstract List<? extends Server> createWorkerServers(
            List<? extends SocketAddress> workerAddresses,
            List<WorkerMessageServerCallback> workerServerCallbacks,
            Integer maxObjectSize
    );

    protected abstract void createWorkerClientsForWorkerServerCallbacks(
            List<? extends SocketAddress> workerAddresses,
            List<WorkerMessageServerCallback> workerServerCallbacks,
            Integer maxObjectSize,
            List<? extends EventLoopGroup> workerServerCallbackClientEventGroups
    );

    protected abstract List<? extends Client> createWorkerClients(
            List<? extends SocketAddress> workerAddresses,
            Integer maxObjectSize,
            EventLoopGroup eventLoopGroup
    );

    public <R> R submit(WorkerStatelessTask<R> workerStatelessTask, Gem gem) {
        WorkerMessage<R> workerMessage = new WorkerMessage<>(workerStatelessTask, gem);
        int workerIndex = random.nextInt(nWorkers);
        Client workerClient = workerClients.get(workerIndex);

        UUID id = workerClient.send(workerMessage);

        return workerClient.getResponse(id);
    }

    @Override
    public void close() {
        List<Future<?>> workerServerCallbackClientEventGroupsFutures = new ArrayList<>();
        for (int i = 0; i < nWorkers; ++i) {
            workerClients.get(i).close();
            workerServerCallbacks.get(i).close();
            workerServers.get(i).close();
            try {
                workerThreads.get(i).join();
                workerServerCallbackClientEventGroupsFutures.add(workerServerCallbackClientEventGroups.get(i).shutdownGracefully());
            } catch (InterruptedException e) {
                LOGGER.warn("Interrupted: " + e.getMessage(), e);
            }
        }
        try {
            Future<?> workerClientsEventGroupFuture = workerClientsEventGroup.shutdownGracefully();
            for (Future<?> future : workerServerCallbackClientEventGroupsFutures) {
                // TODO: [Vasyukevich Andrey, 29.06.2018]: For some reason await() is too slow, it takes seconds to complete
                future.await();
            }
            // TODO: [Vasyukevich Andrey, 29.06.2018]: For some reason await() is too slow, it takes seconds to complete
            workerClientsEventGroupFuture.await();
        } catch (InterruptedException e) {
            LOGGER.warn("Interrupted: " + e.getMessage(), e);
        }
    }
}
