package com.sbt.mipt.gems.worker;

import com.sbt.mipt.gems.message.Gem;
import com.sbt.mipt.gems.task.WorkerStatelessTask;
import com.sbt.mipt.gems.transport.client.Client;
import com.sbt.mipt.gems.transport.server.ServerCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.SocketAddress;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Worker server callback. Concurrently executes received {@link WorkerStatelessTask} from {@link WorkerMessage}
 *
 * @author Vasyukevich Andrey
 */
public class WorkerMessageServerCallback<R> implements ServerCallback<WorkerMessage<R>, R>, AutoCloseable {
    private final static Logger LOGGER = LoggerFactory.getLogger(WorkerMessageServerCallback.class);

    private final SocketAddress workerAddress;
    private final Map<? extends SocketAddress, Set<String>> executedTasksMap;
    private List<? extends Client> otherWorkersClients;

    public WorkerMessageServerCallback(
            SocketAddress workerAddress,
            Map<SocketAddress, Set<String>> executedTasksMap,
            List<? extends Client> otherWorkersClients
    ) {
        this.workerAddress = workerAddress;
        this.executedTasksMap = executedTasksMap;
        this.otherWorkersClients = otherWorkersClients;
    }

    public WorkerMessageServerCallback(SocketAddress workerAddress, Map<SocketAddress, Set<String>> executedTasksMap) {
        this(workerAddress, executedTasksMap, null);
    }

    public List<? extends Client> getOtherWorkersClients() {
        return otherWorkersClients;
    }

    public void setOtherWorkersClients(List<? extends Client> otherWorkersClients) {
        this.otherWorkersClients = otherWorkersClients;
    }

    @Override
    public R call(WorkerMessage<R> workerMessage) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Worker " + workerAddress + " received " + workerMessage);
        }

        Gem gem = workerMessage.getGem();
        WorkerStatelessTask<R> workerStatelessTask = workerMessage.getWorkerStatelessTask();

        List<Gem> gemParts = workerStatelessTask.split(gem, otherWorkersClients.size());
        int nParts = gemParts.size();
        List<R> partResults = nNullsList(nParts);
        List<Client> workerClientForPart = nNullsList(nParts);
        List<UUID> requestIdForPartExecution = nNullsList(nParts);

        if (nParts == 1) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Worker " + workerAddress + " executing received task");
            }

            R partResult = workerStatelessTask.execute(gemParts.get(0));
            partResults.set(0, partResult);
        }

        while (getFirstPendingPartIndex(nParts, partResults, workerClientForPart) != -1) {
            for (Client workerClient : otherWorkersClients) {
                SocketAddress otherWorkerAddress = workerClient.getConnectedAddress();
                Set<String> workerExecutedTasks = executedTasksMap.get(otherWorkerAddress);

                if (workerExecutedTasks.add(workerStatelessTask.getId())) {
                    int firstPendingPartIndex = getFirstPendingPartIndex(nParts, partResults, workerClientForPart);
                    if (firstPendingPartIndex != -1) {
                        workerClientForPart.set(firstPendingPartIndex, workerClient);

                        WorkerMessage<R> message = new WorkerMessage<>(workerStatelessTask, gemParts.get(firstPendingPartIndex));

                        if (LOGGER.isDebugEnabled()) {
                            LOGGER.debug("Worker " + workerAddress + " sending to " + otherWorkerAddress + " message: " + message);
                        }

                        UUID requestId = workerClient.send(message);

                        requestIdForPartExecution.set(firstPendingPartIndex, requestId);
                    }
                }
            }

            int firstPendingPartIndex = getFirstPendingPartIndex(nParts, partResults, workerClientForPart);
            if (firstPendingPartIndex != -1) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Worker " + workerAddress + " executing part of received task");
                }

                R partResult = workerStatelessTask.execute(gemParts.get(firstPendingPartIndex));
                partResults.set(firstPendingPartIndex, partResult);
            }
        }

        for (int i = 0; i < nParts; ++i) {
            if (partResults.get(i) == null) {
                Client workerClient = workerClientForPart.get(i);
                UUID requestId = requestIdForPartExecution.get(i);
                partResults.set(i, workerClient.getResponse(requestId));
            }
        }

        R result = workerStatelessTask.join(gem, partResults);

        executedTasksMap.get(workerAddress).remove(workerStatelessTask.getId());

        return result;
    }

    private int getFirstPendingPartIndex(int nParts, List<R> partResults, List<Client> workerClientForPart) {
        return IntStream.range(0, nParts)
                .filter(i -> partResults.get(i) == null && workerClientForPart.get(i) == null)
                .findFirst().orElse(-1);
    }

    @Override
    public void close() {
        if (otherWorkersClients != null) {
            otherWorkersClients.forEach(Client::close);
        }
    }

    private <T> List<T> nNullsList(int n) {
        return IntStream.range(0, n).mapToObj(i -> (T) null).collect(Collectors.toList());
    }
}
