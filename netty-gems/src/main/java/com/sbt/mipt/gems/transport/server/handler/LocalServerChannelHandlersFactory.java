package com.sbt.mipt.gems.transport.server.handler;

import com.sbt.mipt.gems.transport.server.ServerCallback;
import io.netty.channel.ChannelHandler;

/**
 * Creates channel handlers for local server
 *
 * @author Vasyukevich Andrey
 */
public class LocalServerChannelHandlersFactory implements ServerChannelHandlersFactory {
    private final ServerCallback callback;

    public LocalServerChannelHandlersFactory(ServerCallback callback) {
        this.callback = callback;
    }

    @Override
    public ChannelHandler[] createServerChannelHandlers() {
        ChannelHandler[] handlers = {
                new ServerMessageHandler(callback)
        };

        return handlers;
    }
}
