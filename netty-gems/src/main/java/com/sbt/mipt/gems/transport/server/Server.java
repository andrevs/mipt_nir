package com.sbt.mipt.gems.transport.server;

import com.sbt.mipt.gems.transport.server.handler.ServerChannelHandlersFactory;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.SocketAddress;

/**
 * Transport server
 *
 * @author Vasyukevich Andrey
 */
public class Server implements Runnable, AutoCloseable {
    private final static Logger LOGGER = LoggerFactory.getLogger(Server.class);

    private final SocketAddress serverAddress;

    private final EventLoopGroup group;
    private final Class<? extends ServerChannel> serverChannelClass;
    private final ServerChannelHandlersFactory serverChannelHandlersFactory;

    private Channel channel;

    protected Server(
            SocketAddress serverAddress,
            EventLoopGroup group,
            Class<? extends ServerChannel> serverChannelClass,
            ServerChannelHandlersFactory serverChannelHandlersFactory
    ) {
        this.serverAddress = serverAddress;
        this.group = group;
        this.serverChannelClass = serverChannelClass;
        this.serverChannelHandlersFactory = serverChannelHandlersFactory;
    }

    @Override
    public void run() {
        try {
            ServerBootstrap serverBootstrap = new ServerBootstrap();

            serverBootstrap.group(group)
                    .channel(serverChannelClass)
                    .childHandler(new ChannelInitializer<AbstractChannel>() {
                        @Override
                        public void initChannel(AbstractChannel ch) throws Exception {
                            ch.pipeline().addLast(serverChannelHandlersFactory.createServerChannelHandlers());
                        }
                    });

            // Start the server.
            ChannelFuture channelFuture = serverBootstrap.bind(serverAddress).sync();

            channel = channelFuture.channel();

            // Wait until the server socket is closed.
            channel.closeFuture().sync();

        } catch (InterruptedException e) {
            LOGGER.warn("Interrupted: " + e.getMessage(), e);
        } finally {
            try {
                // TODO: [Vasyukevich Andrey, 29.06.2018]: For some reason await() is too slow, it takes seconds to complete
                group.shutdownGracefully().await();
            } catch (InterruptedException e) {
                LOGGER.warn("Interrupted: " + e.getMessage(), e);
            }
        }
    }

    public SocketAddress getServerAddress() {
        return serverAddress;
    }

    @Override
    public void close() {
        channel.close();
    }
}
