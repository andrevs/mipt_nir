package com.sbt.mipt.gems.transport.server;

/**
 * Callback interface. Calls on object in received message.
 *
 * @author Vasyukevich Andrey
 */
public interface ServerCallback<T, R> {
    R call(T object);
}
