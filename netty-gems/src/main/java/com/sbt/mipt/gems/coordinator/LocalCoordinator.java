package com.sbt.mipt.gems.coordinator;

import com.sbt.mipt.gems.transport.client.LocalClient;
import com.sbt.mipt.gems.transport.server.LocalServer;
import com.sbt.mipt.gems.worker.WorkerMessageServerCallback;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.local.LocalAddress;

import java.net.SocketAddress;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Coordinator implementation for {@link ConnectionType#LOCAL}
 *
 * @author Vasyukevich Andrey
 */
public class LocalCoordinator extends Coordinator {

    LocalCoordinator(int nWorkers) {
        super(nWorkers, null);
    }

    @Override
    protected List<LocalAddress> createWorkerAddresses() {
        return IntStream.range(0, getnWorkers())
                .mapToObj(i -> new LocalAddress("id_" + i))
                .collect(Collectors.toList());
    }

    @Override
    protected List<LocalServer> createWorkerServers(
            List<? extends SocketAddress> workerAddresses,
            List<WorkerMessageServerCallback> workerServerCallbacks,
            Integer maxObjectSize
    ) {
        List<LocalServer> workerServers = IntStream.range(0, getnWorkers())
                .mapToObj(i -> new LocalServer(workerAddresses.get(i), workerServerCallbacks.get(i)))
                .collect(Collectors.toList());

        return workerServers;
    }

    @Override
    protected void createWorkerClientsForWorkerServerCallbacks(
            List<? extends SocketAddress> workerAddresses,
            List<WorkerMessageServerCallback> workerServerCallbacks,
            Integer maxObjectSize,
            List<? extends EventLoopGroup> workerServerCallbackClientEventGroups
    ) {
        for (int i = 0; i < getnWorkers(); ++i) {
            SocketAddress workerAddress = workerAddresses.get(i);
            EventLoopGroup eventLoopGroup = workerServerCallbackClientEventGroups.get(i);
            List<LocalClient> otherWorkersClients = workerAddresses.stream()
                    .filter(addr -> !workerAddress.equals(addr))
                    .map(addr -> new LocalClient(addr, eventLoopGroup))
                    .collect(Collectors.toList());

            workerServerCallbacks.get(i).setOtherWorkersClients(otherWorkersClients);
        }
    }

    @Override
    protected List<LocalClient> createWorkerClients(
            List<? extends SocketAddress> workerAddresses,
            Integer maxObjectSize,
            EventLoopGroup eventLoopGroup
    ) {
        return workerAddresses.stream().map(addr -> new LocalClient(addr, eventLoopGroup)).collect(Collectors.toList());
    }
}
