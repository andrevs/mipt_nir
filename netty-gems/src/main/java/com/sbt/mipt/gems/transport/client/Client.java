package com.sbt.mipt.gems.transport.client;

import com.sbt.mipt.gems.transport.Message;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.SocketAddress;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Transport client
 *
 * @author Vasyukevich Andrey
 */
public class Client implements AutoCloseable {
    private final static Logger LOGGER = LoggerFactory.getLogger(Client.class);

    private final SocketAddress connectedAddress;
    private final EventLoopGroup group;
    private final Class<? extends Channel> channelClass;
    private final Map<UUID, BlockingQueue<Object>> responseMap = new HashMap<>();
    private final List<ChannelHandler> channelHandlers = new ArrayList<>();

    private Channel channel;

    protected Client(
            SocketAddress connectedAddress,
            EventLoopGroup group,
            Class<? extends Channel> channelClass
    ) {
        this.connectedAddress = connectedAddress;
        this.group = group;
        this.channelClass = channelClass;
    }

    protected Map<UUID, BlockingQueue<Object>> getResponseMap() {
        return responseMap;
    }

    protected List<ChannelHandler> getChannelHandlers() {
        return channelHandlers;
    }

    protected void init() {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.group(group)
                .channel(channelClass)
                .handler(new ChannelInitializer<AbstractChannel>() {
                    @Override
                    public void initChannel(AbstractChannel ch) throws Exception {
                        ch.pipeline().addLast(channelHandlers.toArray(new ChannelHandler[0]));
                    }
                });

        try {
            ChannelFuture f = bootstrap.connect(connectedAddress).await();
            channel = f.channel();
        } catch (InterruptedException e) {
            LOGGER.warn("Interrupted: " + e.getMessage(), e);
            close();
        }
    }

    public <T> UUID send(T object) {
        Message<T> message = new Message<>(object);
        UUID requestId = message.getId();

        responseMap.put(requestId, new LinkedBlockingQueue<>());

        channel.writeAndFlush(message);

        return requestId;
    }

    public <R> R getResponse(UUID requestId) {
        try {
            BlockingQueue<Object> responseBlockingQueue = responseMap.get(requestId);

            if (responseBlockingQueue == null) {
                throw new IllegalArgumentException("Unknown requestId: " + requestId);
            }

            R response = (R) responseBlockingQueue.take();

            responseMap.remove(requestId);

            return response;

        } catch (InterruptedException e) {
            LOGGER.warn("Interrupted: " + e.getMessage(), e);
            return null;
        }
    }

    public SocketAddress getConnectedAddress() {
        return connectedAddress;
    }

    @Override
    public void close() {
        channel.close();
    }
}
