package com.sbt.mipt.gems.transport.client;

import com.sbt.mipt.gems.transport.client.handler.ClientMessageHandler;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.local.LocalChannel;

import java.net.SocketAddress;

/**
 * Client implementation for local connection (in JVM)
 *
 * @author Vasyukevich Andrey
 */
public class LocalClient extends Client {
    public LocalClient(SocketAddress serverAddress, EventLoopGroup eventLoopGroup) {
        super(serverAddress, eventLoopGroup, LocalChannel.class);

        getChannelHandlers().add(new ClientMessageHandler(getResponseMap()));

        init();
    }
}
