package com.sbt.mipt.gems.transport.server.handler;

import io.netty.channel.ChannelHandler;

/**
 * Creates new instance of server handlers for new connection
 *
 * @author Vasyukevich Andrey
 */
public interface ServerChannelHandlersFactory {
    ChannelHandler[] createServerChannelHandlers();
}
