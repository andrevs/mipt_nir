package com.sbt.mipt.gems.coordinator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Coordinator factory
 *
 * @author Vasyukevich Andrey
 */
public class CoordinatorFactory {
    private final static Logger LOGGER = LoggerFactory.getLogger(CoordinatorFactory.class);

    /**
     * Creates {@link Coordinator} for given {@link ConnectionType} with given number of workers and given max object size
     * @param maxObjectSize used only for {@link InetCoordinator} creation. If null, then used default max object size.
     */
    public static Coordinator createCoordinator(int nWorkers, ConnectionType connectionType, Integer maxObjectSize) {
        LOGGER.info("Create coordinator for params: nWorkers={}, connectionType={}, maxObjectSize={}", nWorkers, connectionType, maxObjectSize);

        switch (connectionType) {
            case INET:
                return new InetCoordinator(nWorkers, maxObjectSize);
            case LOCAL:
                return new LocalCoordinator(nWorkers);
            default:
                throw new IllegalArgumentException("Unknown connection type: " + connectionType.name());
        }
    }
}
