package com.sbt.mipt.gems.transport.server.handler;

import com.sbt.mipt.gems.transport.server.ServerCallback;
import io.netty.channel.ChannelHandler;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;

/**
 * Creates channel handlers for inet server
 *
 * @author Vasyukevich Andrey
 */
public class InetServerChannelHandlersFactory implements ServerChannelHandlersFactory {
    private final ServerCallback callback;
    private final Integer maxObjectSize;

    public InetServerChannelHandlersFactory(ServerCallback callback, int maxObjectSize) {
        this.callback = callback;
        this.maxObjectSize = maxObjectSize;
    }

    public InetServerChannelHandlersFactory(ServerCallback callback) {
        this.callback = callback;
        this.maxObjectSize = null;
    }

    @Override
    public ChannelHandler[] createServerChannelHandlers() {
        ChannelHandler[] handlers = {
                new ObjectEncoder(),
                createObjectDecoder(),
                new ServerMessageHandler(callback)
        };

        return handlers;
    }

    private ChannelHandler createObjectDecoder() {
        if (maxObjectSize == null) {
            return new ObjectDecoder(ClassResolvers.cacheDisabled(null));
        } else {
            return new ObjectDecoder(maxObjectSize, ClassResolvers.cacheDisabled(null));
        }
    }
}
