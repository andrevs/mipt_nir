package com.sbt.mipt.gems.transport.server.handler;

import com.sbt.mipt.gems.transport.Message;
import com.sbt.mipt.gems.transport.server.ServerCallback;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Server message handler
 *
 * @author Vasyukevich Andrey
 */
public class ServerMessageHandler extends ChannelInboundHandlerAdapter {
    private final static Logger LOGGER = LoggerFactory.getLogger(ServerMessageHandler.class);

    private final ServerCallback callback;

    public ServerMessageHandler(ServerCallback callback) {
        this.callback = callback;
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("ServerMessageHandler read " + msg);
        }

        Message<?> message = (Message<?>) msg;

        Object result = callback.call(message.getObject());

        ctx.write(message.newLinkedMessage(result));
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) {
        ctx.flush();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        cause.printStackTrace();
        ctx.close();
    }
}
