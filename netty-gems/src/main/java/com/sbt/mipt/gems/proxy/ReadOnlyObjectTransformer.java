package com.sbt.mipt.gems.proxy;

import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.math3.complex.Complex;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.Spliterator;
import java.util.stream.Stream;

/**
 * Makes objects read-only.
 *
 * @author Vasyukevich Andrey
 */
public class ReadOnlyObjectTransformer {
    public static <T> T makeReadOnly(T object) {
        if (isObjectImmutable(object)) {
            return object;
        }

        return (T) getReadOnlyProxy(object);
    }

    public static class ReadOnlyInvocationHandler implements InvocationHandler {
        private Object target;

        public ReadOnlyInvocationHandler(Object target) {
            this.target = target;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            if (isMethodReadOnly(method)) {
                Object result = method.invoke(target, args);

                if (isObjectImmutable(result)) {
                    return result;
                }

                return getReadOnlyProxy(result);

            } else {
                throw new IllegalAccessException("Access to non-read-only methods is forbidded: " + method + ". " +
                        "Current implementation allows only getters (methods which start with \"get\") and " +
                        allowedMethodsNames + " methods.");
            }
        }
    }

    private static Object getReadOnlyProxy(Object object) {
        Class<?> clazz = object.getClass();

        if (object instanceof Object[]) {
            Object[] array = (Object[]) object;
            Object[] readOnlyArray = new Object[array.length];
            for (int i = 0; i < array.length; ++i) {
                readOnlyArray[i] = (isObjectImmutable(array[i]) ? array[i] : getReadOnlyProxy(array[i]));
            }
            return readOnlyArray;
        }

        HashSet<Class<?>> interfacesSet = new HashSet<>(ClassUtils.getAllInterfaces(clazz));
        interfacesSet.add(Immutable.class);

        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        Class[] interfaces = interfacesSet.toArray(new Class[0]);
        ReadOnlyInvocationHandler invocationHandler = new ReadOnlyInvocationHandler(object);

        return Proxy.newProxyInstance(classLoader, interfaces, invocationHandler);
    }

    private static boolean isObjectImmutable(Object object) {
        return (object == null
                || (object instanceof Immutable)
                || (object instanceof Boolean)
                || (object instanceof Number)
                || (object instanceof String)
                || (object instanceof Enum)
                || (object instanceof Stream)      // warn
                || (object instanceof Spliterator) // warn
                || (object instanceof Throwable)
                || (object instanceof Class)
                || (object instanceof Complex));
    }

    private static boolean isMethodReadOnly(Method method) {
        String name = method.getName();
        return (name.startsWith("get") || allowedMethodsNames.contains(name));
    }

    private static Set<String> allowedMethodsNames = new HashSet<>(
            Arrays.asList(
                    "toString", "hashCode", "clone", "equals",                    // java.lang.Object
                    "size", "isEmpty", "contains", "iterator", "toArray",
                    "containsAll", "spliterator", "stream", "parallelStream", // java.util.Collection
                    "listIterator", "subList",                                    // java.util.List
                    "hasNext", "next", "forEachRemaining"                         // java.util.Iterator
            )
    );

//    private static boolean isGetter(Method method) {
//        return (method.getName().startsWith("get")
//                && method.getParameterCount() == 0
//                && !Void.TYPE.equals(method.getReturnType()));
//    }
}
