package com.sbt.mipt.gems.transport.server;

import com.sbt.mipt.gems.transport.server.handler.InetServerChannelHandlersFactory;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

import java.net.SocketAddress;

/**
 * Server implementation for inet connections
 *
 * @author Vasyukevich Andrey
 */
public class InetServer extends Server {
    public InetServer(SocketAddress serverAddress, ServerCallback callback, int maxObjectSize) {
        super(
                serverAddress,
                new NioEventLoopGroup(),
                NioServerSocketChannel.class,
                new InetServerChannelHandlersFactory(callback, maxObjectSize)
        );
    }

    public InetServer(SocketAddress serverAddress, ServerCallback callback) {
        super(
                serverAddress,
                new NioEventLoopGroup(),
                NioServerSocketChannel.class,
                new InetServerChannelHandlersFactory(callback)
        );
    }
}
