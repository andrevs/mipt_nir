package com.sbt.mipt.gems.message;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * GEM. Holds data, which used in interaction between workers.
 *
 * @author Vasyukevich Andrey
 */
public class Gem implements Serializable {
    private final GemAccessMode gemAccessMode;
    private final Map<String, Object> properties = new HashMap<>();

    public Gem(GemAccessMode gemAccessMode) {
        this.gemAccessMode = gemAccessMode;
    }

    public <T> T get(String propertyName) {
        return (T) properties.get(propertyName);
    }

    public <T> void put(String propertyName, T propertyValue) {
        if (GemAccessMode.READ_ONLY.equals(gemAccessMode) && properties.containsKey(propertyName)) {
            throw new IllegalStateException("Gem is read-only, it's forbidded to overwrite properties!");
        } else {
            properties.put(propertyName, ProxyFactoryForGem.createProxy(propertyValue, gemAccessMode));
        }
    }

    public GemAccessMode getGemAccessMode() {
        return gemAccessMode;
    }

    @Override
    public String toString() {
        return "Gem(gemAccessMode=" + gemAccessMode + ")";
    }
}
