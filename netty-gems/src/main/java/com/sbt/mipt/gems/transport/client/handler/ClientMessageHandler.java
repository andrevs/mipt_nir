package com.sbt.mipt.gems.transport.client.handler;

import com.sbt.mipt.gems.transport.Message;
import com.sbt.mipt.gems.transport.server.handler.ServerMessageHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;

/**
 * Client message handler
 *
 * @author Vasyukevich Andrey
 */
public class ClientMessageHandler extends SimpleChannelInboundHandler<Object> {
    private final static Logger LOGGER = LoggerFactory.getLogger(ServerMessageHandler.class);

    private final Map<UUID, BlockingQueue<Object>> responseMap;

    public ClientMessageHandler(Map<UUID, BlockingQueue<Object>> responseMap) {
        this.responseMap = responseMap;
    }

    @Override
    public void channelRead0(ChannelHandlerContext ctx, Object msg) throws Exception {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("ClientMessageHandler read " + msg);
        }

        Message<?> message = (Message<?>) msg;
        UUID id = message.getId();
        BlockingQueue<Object> responseBlockingQueue = responseMap.get(id);

        if (responseBlockingQueue == null) {
            throw new IllegalArgumentException("Unknown id: " + id);
        }

        responseBlockingQueue.put(message.getObject());
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        cause.printStackTrace();
        ctx.close();
    }
}
