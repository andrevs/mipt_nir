package com.sbt.mipt.gems.transport.client;

import com.sbt.mipt.gems.transport.client.handler.ClientMessageHandler;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;

import java.net.SocketAddress;
import java.util.Arrays;

/**
 * Client implementation for local connection (in JVM)
 *
 * @author Vasyukevich Andrey
 */
public class InetClient extends Client {
    public InetClient(SocketAddress serverAddress, EventLoopGroup eventLoopGroup, int maxObjectSize) {
        super(serverAddress, eventLoopGroup, NioSocketChannel.class);

        getChannelHandlers().addAll(Arrays.asList(
                new ObjectEncoder(),
                new ObjectDecoder(maxObjectSize, ClassResolvers.cacheDisabled(null)),
                new ClientMessageHandler(getResponseMap())
        ));

        init();
    }

    public InetClient(SocketAddress serverAddress, EventLoopGroup eventLoopGroup) {
        super(serverAddress, eventLoopGroup, NioSocketChannel.class);

        getChannelHandlers().addAll(Arrays.asList(
                new ObjectEncoder(),
                new ObjectDecoder(ClassResolvers.cacheDisabled(null)),
                new ClientMessageHandler(getResponseMap())
        ));

        init();
    }
}
