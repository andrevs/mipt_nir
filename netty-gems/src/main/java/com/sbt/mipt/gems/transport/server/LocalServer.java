package com.sbt.mipt.gems.transport.server;

import com.sbt.mipt.gems.transport.server.handler.LocalServerChannelHandlersFactory;
import io.netty.channel.DefaultEventLoopGroup;
import io.netty.channel.local.LocalServerChannel;

import java.net.SocketAddress;

/**
 * Server implementation for local connection (in JVM)
 *
 * @author Vasyukevich Andrey
 */
public class LocalServer extends Server {
    public LocalServer(SocketAddress serverAddress, ServerCallback callback) {
        super(
                serverAddress,
                new DefaultEventLoopGroup(),
                LocalServerChannel.class,
                new LocalServerChannelHandlersFactory(callback)
        );
    }
}
