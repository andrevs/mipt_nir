package com.sbt.mipt.gems.coordinator;

import com.sbt.mipt.gems.transport.client.InetClient;
import com.sbt.mipt.gems.transport.server.InetServer;
import com.sbt.mipt.gems.transport.server.ServerCallback;
import com.sbt.mipt.gems.worker.WorkerMessageServerCallback;
import io.netty.channel.EventLoopGroup;

import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Coordinator implementation for {@link ConnectionType#INET}
 *
 * @author Vasyukevich Andrey
 */
public class InetCoordinator extends Coordinator {

    public InetCoordinator(int nWorkers, Integer maxObjectSize) {
        super(nWorkers, maxObjectSize);
    }

    @Override
    protected List<InetSocketAddress> createWorkerAddresses() {
        return IntStream.range(0, getnWorkers())
                .mapToObj(i -> new InetSocketAddress("localhost", 8000 + i))
                .collect(Collectors.toList());
    }

    @Override
    protected List<InetServer> createWorkerServers(
            List<? extends SocketAddress> workerAddresses,
            List<WorkerMessageServerCallback> workerServerCallbacks,
            Integer maxObjectSize
    ) {
        List<InetServer> workerServers = IntStream.range(0, getnWorkers())
                .mapToObj(i -> createInetServer(workerAddresses.get(i), workerServerCallbacks.get(i), maxObjectSize))
                .collect(Collectors.toList());

        return workerServers;
    }

    private InetServer createInetServer(SocketAddress serverAddress, ServerCallback callback, Integer maxObjectSize) {
        if (maxObjectSize == null) {
            return new InetServer(serverAddress, callback);
        } else {
            return new InetServer(serverAddress, callback, maxObjectSize);
        }
    }

    @Override
    protected void createWorkerClientsForWorkerServerCallbacks(
            List<? extends SocketAddress> workerAddresses,
            List<WorkerMessageServerCallback> workerServerCallbacks,
            Integer maxObjectSize,
            List<? extends EventLoopGroup> workerServerCallbackClientEventGroups
    ) {
        for (int i = 0; i < getnWorkers(); ++i) {
            SocketAddress workerAddress = workerAddresses.get(i);
            EventLoopGroup eventLoopGroup = workerServerCallbackClientEventGroups.get(i);
            List<InetClient> otherWorkersClients = workerAddresses.stream()
                    .filter(addr -> !workerAddress.equals(addr))
                    .map(addr -> (maxObjectSize != null ? new InetClient(addr, eventLoopGroup, maxObjectSize) : new InetClient(addr, eventLoopGroup)))
                    .collect(Collectors.toList());

            workerServerCallbacks.get(i).setOtherWorkersClients(otherWorkersClients);
        }
    }

    @Override
    protected List<InetClient> createWorkerClients(
            List<? extends SocketAddress> workerAddresses,
            Integer maxObjectSize,
            EventLoopGroup eventLoopGroup
    ) {
        return workerAddresses.stream()
                .map(addr -> (maxObjectSize != null ? new InetClient(addr, eventLoopGroup, maxObjectSize) : new InetClient(addr, eventLoopGroup)))
                .collect(Collectors.toList());
    }
}
