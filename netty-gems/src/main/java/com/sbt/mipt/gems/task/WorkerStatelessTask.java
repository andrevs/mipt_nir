package com.sbt.mipt.gems.task;

import com.sbt.mipt.gems.message.Gem;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

/**
 * Interface for executing task using workers.
 *
 * @author Vasyukevich Andrey
 */
public abstract class WorkerStatelessTask<R> implements Serializable {
    private final String id = UUID.randomUUID().toString();

    /**
     * Returns unique task id. Need to avoid deadlocks.
     */
    public final String getId() {
        return id;
    }

    /**
     * Execute task on given data (from gem)
     */
    public abstract R execute(Gem gem);

    /**
     * Split task data into list of subtasks data
     */
    public abstract List<Gem> split(Gem gem, int nWorkers);

    /**
     * Restore task result from subtasks results
     */
    public abstract R join(Gem gem, List<R> results);

    @Override
    public String toString() {
        return "WorkerStatelessTask(id=" + id + ")";
    }
}
