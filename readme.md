# GEMs model

Message-passing Actors (or Web-Workers) model with shared memory for Java. Based on Netty framework.

**Tags:** `Java` `Netty` `GEMs` `Shared Messages` `Event Loop` `Actors` `Web-Workers` 

### Modules

- #### `netty-gems`
  Contains implementation of gems model (based on netty).

- #### `netty-gems-examples`
  Contains examples of using this gems model implementation.


### Changelog

#### [0.0.3]
- Changed work with event loop groups to more optimal way

#### [0.0.2]
- Changed model to use new transport and easy switch from LOCAL to INET and vice versa
- Gems examples modified to easy change run configuration via command line
- Added primes, mandelbrot, wordcount, hash, sort examples

#### [0.0.1]
- Added transport implementation
- Added classes for read-only gem support
- Added jar-with-dependencies and global Main for examples package
- Added grep example
- Added readme.md
- Make WorkerTask serializable
- Added artifact with examples; added first example &#8211; factorial computing
- Created gems model implementation
- Created base pom file of gems-parent artifact
