package com.sbt.mipt.gems.examples.primes;

import com.sbt.mipt.gems.coordinator.ConnectionType;
import com.sbt.mipt.gems.coordinator.Coordinator;
import com.sbt.mipt.gems.coordinator.CoordinatorFactory;
import com.sbt.mipt.gems.message.Gem;
import com.sbt.mipt.gems.message.GemAccessMode;
import org.junit.Test;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.assertFalse;

/**
 * Test execution of {@link PrimesTask}
 *
 * @author Vasyukevich Andrey
 */
public class PrimesExampleExecutionTest {

    @Test(timeout = 30000)
    public void test() {
        Coordinator coordinator = CoordinatorFactory.createCoordinator(3, ConnectionType.LOCAL, null);

        List<Integer> numbers = IntStream.range(1, 100).boxed().collect(Collectors.toList());

        PrimesTask primesTask = new PrimesTask();
        Gem primesGem = PrimesTask.createGem(numbers, 10, GemAccessMode.DEFAULT);

        List<Integer> result = coordinator.submit(primesTask, primesGem);
        Set<Integer> resultSet = new HashSet<>(result);

        for (Integer number : numbers) {
            assertFalse(PrimesTask.isPrime(number) ^ resultSet.contains(number));
        }

        coordinator.close();
    }
}
