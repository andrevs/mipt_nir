package com.sbt.mipt.gems.examples.hash;

import com.sbt.mipt.gems.coordinator.ConnectionType;
import com.sbt.mipt.gems.coordinator.Coordinator;
import com.sbt.mipt.gems.coordinator.CoordinatorFactory;
import com.sbt.mipt.gems.examples.TextUtils;
import com.sbt.mipt.gems.examples.Utils;
import com.sbt.mipt.gems.examples.wordcount.WordcountTask;
import com.sbt.mipt.gems.message.Gem;
import com.sbt.mipt.gems.message.GemAccessMode;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

/**
 * Test execution of {@link WordcountTask}
 *
 * @author Vasyukevich Andrey
 */
public class HashExampleExecutionTest {
    private final String textFileName = Paths.get("texts", "text.txt").toString();

    @Test(timeout = 30000)
    public void test() throws IOException {
        File tempDirectory = Files.createTempDirectory("files-to-hash").toFile();

        List<String> lines = TextUtils.readFileLines(textFileName, true);
        List<String> texts = Utils.split(lines, lines.size() / 20).stream()
                .map(list -> list.stream().collect(Collectors.joining("\n")))
                .collect(Collectors.toList());
        int nFiles = texts.size();
        List<String> fileNames = new ArrayList<>();
        for (int i = 0; i < nFiles; ++i) {
            File file = new File(tempDirectory, "file-" + i);
            fileNames.add(file.getAbsolutePath());
            try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
                writer.write(texts.get(i));
            }
        }

        try (Coordinator coordinator = CoordinatorFactory.createCoordinator(3, ConnectionType.LOCAL, null)) {
            HashTask hashTask = new HashTask();
            Gem hashGem = HashTask.createGem(fileNames, HashFunctionEnum.SHA256, 1, GemAccessMode.DEFAULT);

            List<String> hashes = hashTask.execute(hashGem);
            List<String> computedHashes = coordinator.submit(hashTask, hashGem);

            assertEquals(hashes, computedHashes);
        }

        FileUtils.deleteDirectory(tempDirectory);
    }
}
