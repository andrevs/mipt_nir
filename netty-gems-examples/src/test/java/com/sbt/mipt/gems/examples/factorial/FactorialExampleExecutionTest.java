package com.sbt.mipt.gems.examples.factorial;

import com.sbt.mipt.gems.coordinator.ConnectionType;
import com.sbt.mipt.gems.coordinator.Coordinator;
import com.sbt.mipt.gems.coordinator.CoordinatorFactory;
import com.sbt.mipt.gems.message.Gem;
import com.sbt.mipt.gems.message.GemAccessMode;
import org.junit.Test;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Test execution of {@link FactorialTask}
 *
 * @author Vasyukevich Andrey
 */
public class FactorialExampleExecutionTest {

    @Test(timeout = 30000)
    public void test() {
        Coordinator coordinator = CoordinatorFactory.createCoordinator(3, ConnectionType.LOCAL, null);

        List<Integer> numbers = Arrays.asList(1, 3, 5, 10, 100, 1000, 10000);

        FactorialTask factorialTask = new FactorialTask();
        Gem factorialGem = FactorialTask.createGem(numbers, 1, GemAccessMode.DEFAULT);

        List<BigInteger> result = coordinator.submit(factorialTask, factorialGem);

        for (int i = 0; i < numbers.size(); ++i) {
            assertEquals(FactorialTask.factorial(numbers.get(i)), result.get(i));
        }

        coordinator.close();
    }
}
