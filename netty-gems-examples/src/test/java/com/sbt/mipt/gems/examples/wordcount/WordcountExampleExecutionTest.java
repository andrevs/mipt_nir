package com.sbt.mipt.gems.examples.wordcount;

import com.sbt.mipt.gems.coordinator.ConnectionType;
import com.sbt.mipt.gems.coordinator.Coordinator;
import com.sbt.mipt.gems.coordinator.CoordinatorFactory;
import com.sbt.mipt.gems.examples.TextUtils;
import com.sbt.mipt.gems.message.Gem;
import com.sbt.mipt.gems.message.GemAccessMode;
import org.apache.commons.lang3.mutable.MutableInt;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * Test execution of {@link WordcountTask}
 *
 * @author Vasyukevich Andrey
 */
public class WordcountExampleExecutionTest {
    private final String textFileName = Paths.get("texts", "text.txt").toString();

    @Test(timeout = 30000)
    public void test() throws IOException {
        try (Coordinator coordinator = CoordinatorFactory.createCoordinator(3, ConnectionType.LOCAL, null)) {
            List<String> lines = TextUtils.readFileLines(textFileName, true);

            WordcountTask wordcountTask = new WordcountTask();
            Gem wordcountGem = WordcountTask.createGem(lines, lines.size() / 10, GemAccessMode.DEFAULT);

            Map<String, MutableInt> wordcounts = wordcountTask.execute(wordcountGem);
            Map<String, MutableInt> computedWordcounts = coordinator.submit(wordcountTask, wordcountGem);

            assertEquals(wordcounts, computedWordcounts);
        }
    }
}
