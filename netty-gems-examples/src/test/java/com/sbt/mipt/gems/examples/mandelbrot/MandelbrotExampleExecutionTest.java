package com.sbt.mipt.gems.examples.mandelbrot;

import com.sbt.mipt.gems.coordinator.ConnectionType;
import com.sbt.mipt.gems.coordinator.Coordinator;
import com.sbt.mipt.gems.coordinator.CoordinatorFactory;
import com.sbt.mipt.gems.message.Gem;
import com.sbt.mipt.gems.message.GemAccessMode;
import org.apache.commons.math3.complex.Complex;
import org.junit.Test;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * Test execution of {@link MandelbrotTask}
 *
 * @author Vasyukevich Andrey
 */
public class MandelbrotExampleExecutionTest {

    @Test(timeout = 30000)
    public void test() {
        Coordinator coordinator = CoordinatorFactory.createCoordinator(3, ConnectionType.LOCAL, null);

        List<Complex> points = MandelbrotTask.generateGrid(-2, 2, -2, 2, 0.01);
        int maxIter = 1000;

        MandelbrotTask mandelbrotTask = new MandelbrotTask();
        Gem mandelbrotGem = MandelbrotTask.createGem(points, maxIter, 1000, GemAccessMode.DEFAULT);

        List<Complex> result = coordinator.submit(mandelbrotTask, mandelbrotGem);
        Set<Complex> resultSet = new HashSet<>(result);

        for (Complex point : points) {
            assertFalse(MandelbrotTask.isInMandelbrotSet(point, maxIter) ^ resultSet.contains(point));
        }

        assertEquals("Estimated mandelbrot set area is wrong!", 1.50659, result.size() * 1.0 / points.size() * 16, 0.01);

        coordinator.close();
    }
}
