package com.sbt.mipt.gems.examples;

import org.apache.commons.cli.ParseException;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

/**
 * Test for {@link BaseTaskRunner}
 *
 * @author Vasyukevich Andrey
 */
public class BaseTaskRunnerTest {
    private final BaseTaskRunner baseTaskRunner = mock(BaseTaskRunner.class, Mockito.CALLS_REAL_METHODS);

    @Test
    public void getNumberOfRunsTest() throws ParseException {
        baseTaskRunner.parseCommandLineArguments(createArgs());
        assertEquals(1, baseTaskRunner.getNumberOfRuns());

        baseTaskRunner.parseCommandLineArguments(createArgs("-nr", "33"));
        assertEquals(33, baseTaskRunner.getNumberOfRuns());

        baseTaskRunner.parseCommandLineArguments(createArgs("--numberOfRuns", "5"));
        assertEquals(5, baseTaskRunner.getNumberOfRuns());
    }

    private String[] createArgs(String... args) {
        return args;
    }
}
