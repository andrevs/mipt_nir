package com.sbt.mipt.gems.examples.sort;

import com.sbt.mipt.gems.coordinator.ConnectionType;
import com.sbt.mipt.gems.coordinator.Coordinator;
import com.sbt.mipt.gems.coordinator.CoordinatorFactory;
import com.sbt.mipt.gems.message.Gem;
import com.sbt.mipt.gems.message.GemAccessMode;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

/**
 * Test execution of {@link SortTask}
 *
 * @author Vasyukevich Andrey
 */
public class SortExampleExecutionTest {

    @Test(timeout = 30000)
    public void test() {
        Coordinator coordinator = CoordinatorFactory.createCoordinator(3, ConnectionType.LOCAL, null);

        Random random = new Random(123);
        List<Integer> numbers = random.ints(100000).boxed().collect(Collectors.toList());

        assertFalse(isSorted(numbers));

        SortTask sortTask = new SortTask();
        Gem sortGem = SortTask.createGem(numbers, 1000000, 1, GemAccessMode.DEFAULT);

        List<Integer> sorted = coordinator.submit(sortTask, sortGem);

        assertTrue(isSorted(sorted));
        assertEquals(sortedCopy(numbers), sorted);

        coordinator.close();
    }

    @Test(timeout = 30000)
    public void testSortReadOnlyGem() {
        Coordinator coordinator = CoordinatorFactory.createCoordinator(3, ConnectionType.LOCAL, null);

        List<Integer> numbers = Arrays.asList(1, 3, -1, 4, 3, 2);

        SortTask sortTask = new SortTask();
        Gem sortGem = SortTask.createGem(numbers, 1, 3, GemAccessMode.READ_ONLY);

        List<Integer> sorted = coordinator.submit(sortTask, sortGem);

        assertTrue(isSorted(sorted));
        assertEquals(sortedCopy(numbers), sorted);

        coordinator.close();
    }

    private boolean isSorted(List<Integer> list) {
        for (int i = 1; i < list.size(); ++i) {
            if (list.get(i - 1) > list.get(i)) {
                return false;
            }
        }
        return true;
    }

    private List<Integer> sortedCopy(List<Integer> numbers) {
        List<Integer> sorted = new ArrayList<>(numbers);

        Collections.sort(sorted);

        return sorted;
    }
}
