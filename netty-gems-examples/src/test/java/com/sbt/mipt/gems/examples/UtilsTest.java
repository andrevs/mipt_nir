package com.sbt.mipt.gems.examples;

import org.junit.Test;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;

/**
 * Test for {@link Utils}
 *
 * @author Vasyukevich Andrey
 */
public class UtilsTest {
    @Test
    public void splitTest() {
        splitTestHelper(IntStream.range(0, 3).boxed().collect(Collectors.toList()), 1);

        splitTestHelper(IntStream.range(0, 2).boxed().collect(Collectors.toList()), 2);
        splitTestHelper(IntStream.range(0, 3).boxed().collect(Collectors.toList()), 2);
        splitTestHelper(IntStream.range(0, 4).boxed().collect(Collectors.toList()), 2);

        splitTestHelper(IntStream.range(0, 40).boxed().collect(Collectors.toList()), 3);
    }

    public <T> void splitTestHelper(List<T> list, int partSize) {
        List<List<T>> parts = Utils.split(list, partSize);

        assertEquals(parts.size(), (list.size() + partSize - 1) / partSize);

        assertEquals(list, parts.stream().flatMap(List::stream).collect(Collectors.toList()));
    }
}
