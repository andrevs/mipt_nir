package com.sbt.mipt.gems.examples.grep;

import com.sbt.mipt.gems.coordinator.ConnectionType;
import com.sbt.mipt.gems.coordinator.Coordinator;
import com.sbt.mipt.gems.coordinator.CoordinatorFactory;
import com.sbt.mipt.gems.examples.TextUtils;
import com.sbt.mipt.gems.message.Gem;
import com.sbt.mipt.gems.message.GemAccessMode;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * Test execution of {@link GrepTask}
 *
 * @author Vasyukevich Andrey
 */
public class GrepExampleExecutionTest {
    private final String filesDir = "texts";
    private final String textFileName = Paths.get(filesDir, "text.txt").toString();
    private final String wordsFileName = Paths.get(filesDir, "words.txt").toString();

    @Test(timeout = 30000)
    public void test() throws IOException {
        try (Coordinator coordinator = CoordinatorFactory.createCoordinator(3, ConnectionType.LOCAL, null)) {
            List<String> lines = TextUtils.readFileLines(textFileName, true);
            List<String> words = TextUtils.readFileLines(wordsFileName, true);

            GrepTask grepTask = new GrepTask();
            Gem grepGem = GrepTask.createGem(lines, words, 0, lines.size() / 10, GemAccessMode.DEFAULT);

            Map<String, List<Integer>> occurencesMap = grepTask.execute(grepGem);
            Map<String, List<Integer>> computedOccurencesMap = coordinator.submit(grepTask, grepGem);

            assertEquals(occurencesMap, computedOccurencesMap);
        }
    }
}
