package com.sbt.mipt.gems.examples;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;

/**
 * Test for {@link TextUtils}
 *
 * @author Vasyukevich Andrey
 */
public class TextUtilsTest {
    @Test
    public void grepTextWordTest() {
        String text = "abcaefffk";
        assertEquals(Arrays.asList(0, 3), TextUtils.grep(text, "a"));
        assertEquals(Collections.singletonList(5), TextUtils.grep(text, "ff"));
        assertEquals(Collections.emptyList(), TextUtils.grep(text, "--"));
    }

    @Test
    public void grepTextPatternTest() {
        String text = "abc";
        assertEquals(Collections.singletonList(0), TextUtils.grep(text, Pattern.compile(text)));
    }

    @Test
    public void grepLinesWordsTest() {
        String text = "asdfghjkl\nqwertyuiopqwertyuiop\nasdfghjkl";
        List<String> lines = Arrays.asList(text.split("\n"));
        List<String> words = Arrays.asList("as", "wer", "--");

        Map<String, List<Integer>> occurences = TextUtils.grep(lines, words);

        for (String word : words) {
            assertEquals(TextUtils.grep(text, word), occurences.get(word));
        }
    }

    @Test
    public void splitIntoWordsTest() {
        String line = "a, bc:b deF a";
        assertEquals(Arrays.asList("a", "bc", "b", "deF", "a"), TextUtils.splitIntoWords(line));
    }
}
