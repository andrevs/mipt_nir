package com.sbt.mipt.gems.examples.primes;

import com.sbt.mipt.gems.examples.Utils;
import com.sbt.mipt.gems.message.Gem;
import com.sbt.mipt.gems.message.GemAccessMode;
import com.sbt.mipt.gems.task.WorkerStatelessTask;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Task for finding prime numbers using gems model.
 *
 * @author Vasyukevich Andrey
 */
public class PrimesTask extends WorkerStatelessTask<List<Integer>> {

    @Override
    public List<Integer> execute(Gem gem) {
        List<Integer> numbers = gem.get("numbers");

        return numbers.stream().filter(PrimesTask::isPrime).collect(Collectors.toList());
    }

    @Override
    public List<Gem> split(Gem gem, int nWorkers) {
        List<Integer> numbers = gem.get("numbers");
        int partSize = gem.get("partSize");

        return Utils.split(numbers, partSize).stream()
                .map(part -> createGem(part, partSize, gem.getGemAccessMode()))
                .collect(Collectors.toList());
    }

    @Override
    public List<Integer> join(Gem gem, List<List<Integer>> results) {
        return results.stream().flatMap(List::stream).collect(Collectors.toList());
    }

    public static Gem createGem(List<Integer> numbers, int partSize, GemAccessMode gemAccessMode) {
        Gem gem = new Gem(gemAccessMode);

        gem.put("numbers", numbers);
        gem.put("partSize", partSize);

        return gem;
    }

    public static boolean isPrime(int n) {
        for (int d = 2; d * d <= n; ++d) {
            if (n % d == 0) {
                return false;
            }
        }

        return true;
    }
}
