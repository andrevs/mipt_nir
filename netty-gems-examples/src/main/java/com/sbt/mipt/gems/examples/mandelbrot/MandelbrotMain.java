package com.sbt.mipt.gems.examples.mandelbrot;

import com.sbt.mipt.gems.examples.BaseTaskRunner;
import com.sbt.mipt.gems.message.Gem;
import org.apache.commons.cli.Options;
import org.apache.commons.math3.complex.Complex;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Main class for computing mandelbrot set using gems model.
 *
 * @author Vasyukevich Andrey
 */
public class MandelbrotMain {
    public static void main(String[] args) throws IOException {
        BaseTaskRunner mandelbrotTaskRunner = new MandelbrotTaskRunner();

        mandelbrotTaskRunner.parseCommandLineArguments(args);

        mandelbrotTaskRunner.runMain();
    }

    public static class MandelbrotTaskRunner extends BaseTaskRunner {
        private List<Complex> points;
        private int maxIter;
        private int partSize;

        public MandelbrotTaskRunner() {
            super("mandelbrot");
        }

        @Override
        public Options createOptions() {
            Options options = super.createOptions();

            options.addOption(createOption(null, "minX", true, false, true,
                    "Minimum x-axis value for grid.", null));
            options.addOption(createOption(null, "maxX", true, false, true,
                    "Maximum x-axis value for grid.", null));
            options.addOption(createOption(null, "minY", true, false, true,
                    "Minimum y-axis value for grid.", null));
            options.addOption(createOption(null, "maxY", true, false, true,
                    "Maximum y-axis value for grid.", null));
            options.addOption(createOption(null, "cellSize", true, false, true,
                    "Grid cell size.", null));
            options.addOption(createOption(null, "maxIter", true, false, true,
                    "Maximum number of iteration to check belonging to Mandelbrot set.", null));
            options.addOption(createOption(null, "partSize", true, false, true,
                    "Part size.", null));

            return options;
        }

        @Override
        public void initTaskParameters() {
            double minX = Double.valueOf(commandLine.getOptionValue("minX"));
            double maxX = Double.valueOf(commandLine.getOptionValue("maxX"));
            double minY = Double.valueOf(commandLine.getOptionValue("minY"));
            double maxY = Double.valueOf(commandLine.getOptionValue("maxY"));
            double cellSize = Double.valueOf(commandLine.getOptionValue("cellSize"));

            points = MandelbrotTask.generateGrid(minX, maxX, minY, maxY, cellSize);
            maxIter = Integer.valueOf(commandLine.getOptionValue("maxIter"));
            partSize = Integer.valueOf(commandLine.getOptionValue("partSize"));
        }

        @Override
        public MandelbrotTask createWorkerStatelessTask() {
            return new MandelbrotTask();
        }

        @Override
        public Gem createTaskGem() {
            return MandelbrotTask.createGem(points, maxIter, partSize, getGemAccessMode());
        }

        @Override
        public List<String> taskResultToListOfStrings(Object result) {
            List<Complex> points = (List<Complex>) result;

            return points.stream().map(String::valueOf).collect(Collectors.toList());
        }
    }
}
