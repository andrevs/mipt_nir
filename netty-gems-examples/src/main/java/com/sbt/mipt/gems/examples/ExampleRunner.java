package com.sbt.mipt.gems.examples;

import com.sbt.mipt.gems.examples.factorial.FactorialMain;
import com.sbt.mipt.gems.examples.grep.GrepMain;
import com.sbt.mipt.gems.examples.hash.HashMain;
import com.sbt.mipt.gems.examples.mandelbrot.MandelbrotMain;
import com.sbt.mipt.gems.examples.primes.PrimesMain;
import com.sbt.mipt.gems.examples.sort.SortMain;
import com.sbt.mipt.gems.examples.wordcount.WordcountMain;
import org.apache.commons.cli.*;

import java.util.Arrays;

/**
 * ExampleRunner for examples artifact.
 *
 * @author Vasyukevich Andrey
 */
public class ExampleRunner {
    public static void main(String[] args) throws Exception {
        Options options = new Options();
        options.addOption(null, "exampleTask", true,
                "Specify example task name. One of: factorial, grep, hash, mandelbrot, primes, sort, wordcount");

        CommandLineParser parser = new DefaultParser();
        HelpFormatter helpFormatter = new HelpFormatter();
        CommandLine commandLine;

        if (args.length < 2) {
            helpFormatter.printHelp("example-task-runner", options);
            System.exit(1);
        }

        String[] runnerArgs = Arrays.copyOfRange(args, 0, 2);

        try {
            commandLine = parser.parse(options, runnerArgs);
        } catch (ParseException e) {
            helpFormatter.printHelp("example-task-runner", options);
            throw e;
        }

        String example = commandLine.getOptionValue("exampleTask");
        String[] exampleArgs = Arrays.copyOfRange(args, 2, args.length);

        switch (example) {
            case "factorial":
                FactorialMain.main(exampleArgs);
                break;
            case "grep":
                GrepMain.main(exampleArgs);
                break;
            case "hash":
                HashMain.main(exampleArgs);
                break;
            case "mandelbrot":
                MandelbrotMain.main(exampleArgs);
                break;
            case "primes":
                PrimesMain.main(exampleArgs);
                break;
            case "sort":
                SortMain.main(exampleArgs);
                break;
            case "wordcount":
                WordcountMain.main(exampleArgs);
                break;
            default:
                helpFormatter.printHelp("example-task-runner", options);
                System.exit(1);
        }
    }
}
