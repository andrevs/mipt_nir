package com.sbt.mipt.gems.examples.factorial;

import com.sbt.mipt.gems.examples.Utils;
import com.sbt.mipt.gems.message.Gem;
import com.sbt.mipt.gems.message.GemAccessMode;
import com.sbt.mipt.gems.task.WorkerStatelessTask;

import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Task for computing factorial using gems model.
 *
 * @author Vasyukevich Andrey
 */
public class FactorialTask extends WorkerStatelessTask<List<BigInteger>> {

    @Override
    public List<BigInteger> execute(Gem gem) {
        List<Integer> numbers = gem.get("numbers");

        return numbers.stream().map(FactorialTask::factorial).collect(Collectors.toList());
    }

    @Override
    public List<Gem> split(Gem gem, int nWorkers) {
        List<Integer> numbers = gem.get("numbers");
        int partSize = gem.get("partSize");

        return Utils.split(numbers, partSize).stream()
                .map(part -> createGem(part, partSize, gem.getGemAccessMode()))
                .collect(Collectors.toList());
    }

    @Override
    public List<BigInteger> join(Gem gem, List<List<BigInteger>> results) {
        return results.stream().flatMap(List::stream).collect(Collectors.toList());
    }

    public static Gem createGem(List<Integer> numbers, int partSize, GemAccessMode gemAccessMode) {
        Gem gem = new Gem(gemAccessMode);

        gem.put("numbers", numbers);
        gem.put("partSize", partSize);

        return gem;
    }

    public static BigInteger factorial(int n) {
        BigInteger result = BigInteger.ONE;

        for (int i = 2; i <= n; ++i) {
            result = result.multiply(BigInteger.valueOf(i));
        }

        return result;
    }
}
