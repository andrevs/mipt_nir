package com.sbt.mipt.gems.examples.factorial;

import com.sbt.mipt.gems.examples.BaseTaskRunner;
import com.sbt.mipt.gems.message.Gem;
import org.apache.commons.cli.Options;

import java.io.IOException;
import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Main class for computing factorial using gems model.
 *
 * @author Vasyukevich Andrey
 */
public class FactorialMain {
    public static void main(String[] args) throws IOException {
        BaseTaskRunner factorialTaskRunner = new FactorialTaskRunner();

        factorialTaskRunner.parseCommandLineArguments(args);

        factorialTaskRunner.runMain();
    }

    public static class FactorialTaskRunner extends BaseTaskRunner {
        private List<Integer> numbers;
        private int partSize;

        public FactorialTaskRunner() {
            super("factorial");
        }

        @Override
        public Options createOptions() {
            Options options = super.createOptions();

            options.addOption(createOption(null, "rangeFirst", true, false, true,
                    "First number for computing factorial.", null));
            options.addOption(createOption(null, "rangeLast", true, false, true,
                    "Last number for computing factorial.", null));
            options.addOption(createOption(null, "partSize", true, false, true,
                    "Part size.", null));

            return options;
        }

        @Override
        public void initTaskParameters() {
            int rangeFirst = Integer.valueOf(commandLine.getOptionValue("rangeFirst"));
            int rangeLast = Integer.valueOf(commandLine.getOptionValue("rangeLast"));

            numbers = IntStream.range(rangeFirst, rangeLast + 1).boxed().collect(Collectors.toList());
            partSize = Integer.valueOf(commandLine.getOptionValue("partSize"));
        }

        @Override
        public FactorialTask createWorkerStatelessTask() {
            return new FactorialTask();
        }

        @Override
        public Gem createTaskGem() {
            return FactorialTask.createGem(numbers, partSize, getGemAccessMode());
        }

        @Override
        public List<String> taskResultToListOfStrings(Object result) {
            List<BigInteger> factorials = (List<BigInteger>) result;

            return IntStream.range(0, numbers.size()).mapToObj(i -> numbers.get(i) + "! = " + factorials.get(i)).collect(Collectors.toList());
        }
    }
}
