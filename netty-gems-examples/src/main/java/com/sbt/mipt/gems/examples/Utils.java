package com.sbt.mipt.gems.examples;

import java.util.ArrayList;
import java.util.List;

/**
 * Utils
 *
 * @author Vasyukevich Andrey
 */
public class Utils {
    public static <T> List<List<T>> split(List<T> list, int partSize) {
        int nParts = (list.size() + partSize - 1) / partSize;
        List<List<T>> parts = new ArrayList<>(nParts);

        for (int i = 0; i < nParts; ++i) {
            parts.add(new ArrayList<>(list.subList(i * partSize, Math.min((i + 1) * partSize, list.size()))));
        }

        return parts;
    }
}
