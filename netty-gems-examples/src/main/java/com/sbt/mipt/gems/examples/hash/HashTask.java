package com.sbt.mipt.gems.examples.hash;

import com.google.common.hash.HashCode;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;
import com.google.common.io.Files;
import com.sbt.mipt.gems.examples.Utils;
import com.sbt.mipt.gems.message.Gem;
import com.sbt.mipt.gems.message.GemAccessMode;
import com.sbt.mipt.gems.task.WorkerStatelessTask;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Task for hashing files using gems model.
 *
 * @author Vasyukevich Andrey
 */
public class HashTask extends WorkerStatelessTask<List<String>> {

    @Override
    public List<String> execute(Gem gem) {
        List<String> fileNames = gem.get("fileNames");
        HashFunctionEnum hashFunctionEnum = gem.get("hashFunctionEnum");

        HashFunction hashFunction;
        switch (hashFunctionEnum) {
            case MD5:
                hashFunction = Hashing.md5();
                break;
            case SHA1:
                hashFunction = Hashing.sha1();
                break;
            case SHA256:
                hashFunction = Hashing.sha256();
                break;
            default:
                throw new RuntimeException("Unknown hash function: " + hashFunctionEnum);
        }

        List<String> hashes = new ArrayList<>();
        for (String fileName : fileNames) {
            try {
                File file = new File(fileName);
                HashCode hash = Files.asByteSource(file).hash(hashFunction);
                hashes.add(hash.toString());
            } catch (IOException e) {
                hashes.add(null);
            }
        }

        return hashes;
    }

    @Override
    public List<Gem> split(Gem gem, int nWorkers) {
        List<String> fileNames = gem.get("fileNames");
        HashFunctionEnum hashFunctionEnum = gem.get("hashFunctionEnum");
        int partSize = gem.get("partSize");

        return Utils.split(fileNames, partSize).stream()
                .map(part -> createGem(part, hashFunctionEnum, partSize, gem.getGemAccessMode()))
                .collect(Collectors.toList());
    }

    @Override
    public List<String> join(Gem gem, List<List<String>> results) {
        return results.stream().flatMap(List::stream).collect(Collectors.toList());
    }

    public static Gem createGem(List<String> fileNames, HashFunctionEnum hashFunctionEnum, int partSize, GemAccessMode gemAccessMode) {
        Gem gem = new Gem(gemAccessMode);

        gem.put("fileNames", fileNames);
        gem.put("hashFunctionEnum", hashFunctionEnum);
        gem.put("partSize", partSize);

        return gem;
    }
}
