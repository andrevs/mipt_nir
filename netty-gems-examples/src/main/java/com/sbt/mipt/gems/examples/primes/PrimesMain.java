package com.sbt.mipt.gems.examples.primes;

import com.sbt.mipt.gems.examples.BaseTaskRunner;
import com.sbt.mipt.gems.message.Gem;
import org.apache.commons.cli.Options;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Main class for finding prime numbers using gems model.
 *
 * @author Vasyukevich Andrey
 */
public class PrimesMain {
    public static void main(String[] args) throws IOException {
        BaseTaskRunner primesTaskRunner = new PrimesTaskRunner();

        primesTaskRunner.parseCommandLineArguments(args);

        primesTaskRunner.runMain();
    }

    public static class PrimesTaskRunner extends BaseTaskRunner {
        private List<Integer> numbers;
        private int partSize;

        public PrimesTaskRunner() {
            super("primes");
        }

        @Override
        public Options createOptions() {
            Options options = super.createOptions();

            options.addOption(createOption(null, "rangeFirst", true, false, true,
                    "First number to check primality.", null));
            options.addOption(createOption(null, "rangeLast", true, false, true,
                    "Last number to check primality.", null));
            options.addOption(createOption(null, "partSize", true, false, true,
                    "Part size.", null));

            return options;
        }

        @Override
        public void initTaskParameters() {
            int rangeFirst = Integer.valueOf(commandLine.getOptionValue("rangeFirst"));
            int rangeLast = Integer.valueOf(commandLine.getOptionValue("rangeLast"));

            numbers = IntStream.range(rangeFirst, rangeLast + 1).boxed().collect(Collectors.toList());
            partSize = Integer.valueOf(commandLine.getOptionValue("partSize"));
        }

        @Override
        public PrimesTask createWorkerStatelessTask() {
            return new PrimesTask();
        }

        @Override
        public Gem createTaskGem() {
            return PrimesTask.createGem(numbers, partSize, getGemAccessMode());
        }

        @Override
        public List<String> taskResultToListOfStrings(Object result) {
            List<Integer> primes = (List<Integer>) result;

            return primes.stream().map(String::valueOf).collect(Collectors.toList());
        }
    }
}
