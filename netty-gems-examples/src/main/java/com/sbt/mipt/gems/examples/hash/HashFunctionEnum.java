package com.sbt.mipt.gems.examples.hash;

/**
 * Hash functions.
 *
 * @author Vasyukevich Andrey
 */
public enum HashFunctionEnum {
    MD5,
    SHA1,
    SHA256
}
