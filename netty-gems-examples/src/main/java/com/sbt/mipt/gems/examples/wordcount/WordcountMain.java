package com.sbt.mipt.gems.examples.wordcount;

import com.sbt.mipt.gems.examples.BaseTaskRunner;
import com.sbt.mipt.gems.examples.TextUtils;
import com.sbt.mipt.gems.message.Gem;
import org.apache.commons.cli.Options;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Main class for word count using gems model.
 *
 * @author Vasyukevich Andrey
 */
public class WordcountMain {
    public static void main(String[] args) throws Exception {
        BaseTaskRunner wordcountTaskRunner = new WordcountTaskRunner();

        wordcountTaskRunner.parseCommandLineArguments(args);

        wordcountTaskRunner.runMain();
    }

    public static class WordcountTaskRunner extends BaseTaskRunner {
        private List<String> lines;
        private int partSize;

        public WordcountTaskRunner() {
            super("wordcount");
        }

        @Override
        public Options createOptions() {
            Options options = super.createOptions();

            options.addOption(createOption(null, "textFile", true, false, true,
                    "Text file.", null));
            options.addOption(createOption(null, "partSize", true, false, true,
                    "Part size.", null));

            return options;
        }

        @Override
        public void initTaskParameters() {
            try {
                lines = TextUtils.readFileLines(commandLine.getOptionValue("textFile"), false);
                partSize = Integer.valueOf(commandLine.getOptionValue("partSize"));
            } catch (IOException e) {
                throw new RuntimeException(e.getMessage(), e);
            }
        }

        @Override
        public WordcountTask createWorkerStatelessTask() {
            return new WordcountTask();
        }

        @Override
        public Gem createTaskGem() {
            return WordcountTask.createGem(lines, partSize, getGemAccessMode());
        }

        @Override
        public List<String> taskResultToListOfStrings(Object result) {
            Map<String, Integer> wordcounts = new TreeMap<>((Map<String, Integer>) result);
            List<String> lines = new ArrayList<>();

            for (Map.Entry<String, Integer> entry : wordcounts.entrySet()) {
                lines.add(entry.getKey() + " " + entry.getValue());
            }

            return lines;
        }
    }
}
