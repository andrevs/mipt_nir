package com.sbt.mipt.gems.examples.sort;

import com.sbt.mipt.gems.examples.BaseTaskRunner;
import com.sbt.mipt.gems.examples.TextUtils;
import com.sbt.mipt.gems.message.Gem;
import org.apache.commons.cli.Options;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Main class for sorting using gems model.
 *
 * @author Vasyukevich Andrey
 */
public class SortMain {
    public static void main(String[] args) throws IOException {
        BaseTaskRunner sortTaskRunner = new SortTaskRunner();

        sortTaskRunner.parseCommandLineArguments(args);

        sortTaskRunner.runMain();
    }

    public static class SortTaskRunner extends BaseTaskRunner {
        private List<Integer> numbers;
        private int maxDepth;
        private int minPartToSplit;

        public SortTaskRunner() {
            super("sort");
        }

        @Override
        public Options createOptions() {
            Options options = super.createOptions();

            options.addOption(createOption(null, "fileWithArray", true, false, true,
                    "File with array to sort.", null));
            options.addOption(createOption(null, "maxDepth", true, false, false,
                    "Maximum recursion depth.", "1000000"));
            options.addOption(createOption(null, "minPartToSplit", true, false, false,
                    "Minimum part to parallel split.", "1"));

            return options;
        }

        @Override
        public void initTaskParameters() {
            try {
                String fileWithArray = commandLine.getOptionValue("fileWithArray");
                numbers = TextUtils.readFileLines(fileWithArray, false).stream()
                        .flatMap(line -> Arrays.stream(line.split(" ")).map(Integer::valueOf))
                        .collect(Collectors.toList());
                maxDepth = Integer.valueOf(commandLine.getOptionValue("maxDepth", "1000000"));
                minPartToSplit = Integer.valueOf(commandLine.getOptionValue("minPartToSplit", "1"));
            } catch (IOException e) {
                throw new RuntimeException(e.getMessage(), e);
            }
        }

        @Override
        public SortTask createWorkerStatelessTask() {
            return new SortTask();
        }

        @Override
        public Gem createTaskGem() {
            return SortTask.createGem(numbers, maxDepth, minPartToSplit, getGemAccessMode());
        }

        @Override
        public List<String> taskResultToListOfStrings(Object result) {
            List<Integer> numbers = (List<Integer>) result;

            return Collections.singletonList(numbers.stream().map(String::valueOf).collect(Collectors.joining(" ")));
        }
    }
}
