package com.sbt.mipt.gems.examples.hash;

import com.sbt.mipt.gems.examples.BaseTaskRunner;
import com.sbt.mipt.gems.examples.TextUtils;
import com.sbt.mipt.gems.message.Gem;
import org.apache.commons.cli.Options;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Main class for hashing files using gems model.
 *
 * @author Vasyukevich Andrey
 */
public class HashMain {
    public static void main(String[] args) throws Exception {
        BaseTaskRunner hashTaskRunner = new HashTaskRunner();

        hashTaskRunner.parseCommandLineArguments(args);

        hashTaskRunner.runMain();
    }

    public static class HashTaskRunner extends BaseTaskRunner {
        private List<String> fileNames;
        private HashFunctionEnum hashFunctionEnum;
        private int partSize;

        public HashTaskRunner() {
            super("hash");
        }

        @Override
        public Options createOptions() {
            Options options = super.createOptions();

            options.addOption(createOption(null, "fileWithFilenames", true, false, true,
                    "File with filenames.", null));
            options.addOption(createOption(null, "hashFunction", true, false, true,
                    "Hash function name. One of: MD5, SHA1, SHA256.", null));
            options.addOption(createOption(null, "partSize", true, false, true,
                    "Part size.", null));

            return options;
        }

        @Override
        public void initTaskParameters() {
            try {
                fileNames = TextUtils.readFileLines(commandLine.getOptionValue("fileWithFilenames"), false);
                hashFunctionEnum = HashFunctionEnum.valueOf(commandLine.getOptionValue("hashFunction"));
                partSize = Integer.valueOf(commandLine.getOptionValue("partSize"));
            } catch (IOException e) {
                throw new RuntimeException(e.getMessage(), e);
            }
        }

        @Override
        public HashTask createWorkerStatelessTask() {
            return new HashTask();
        }

        @Override
        public Gem createTaskGem() {
            return HashTask.createGem(fileNames, hashFunctionEnum, partSize, getGemAccessMode());
        }

        @Override
        public List<String> taskResultToListOfStrings(Object result) {
            List<String> hashes = (List<String>) result;

            return IntStream.range(0, fileNames.size())
                    .mapToObj(i -> fileNames.get(i) + ": " + hashFunctionEnum + "=" + hashes.get(i))
                    .collect(Collectors.toList());
        }
    }
}
