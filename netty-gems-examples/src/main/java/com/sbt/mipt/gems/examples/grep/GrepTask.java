package com.sbt.mipt.gems.examples.grep;

import com.sbt.mipt.gems.examples.TextUtils;
import com.sbt.mipt.gems.examples.Utils;
import com.sbt.mipt.gems.message.Gem;
import com.sbt.mipt.gems.message.GemAccessMode;
import com.sbt.mipt.gems.task.WorkerStatelessTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Task for computing grep using gems model.
 *
 * @author Vasyukevich Andrey
 */
public class GrepTask extends WorkerStatelessTask<Map<String, List<Integer>>> {

    @Override
    public Map<String, List<Integer>> execute(Gem gem) {
        List<String> lines = gem.get("lines");
        List<String> words = gem.get("words");
        int offset = gem.get("offset");

        Map<String, List<Integer>> occurences = TextUtils.grep(lines, words);
        Map<String, List<Integer>> occurencesWithOffset = new HashMap<>();

        for (Map.Entry<String, List<Integer>> entry : occurences.entrySet()) {
            List<Integer> wordOccurences = entry.getValue();
            List<Integer> wordOccurencesWithOffset = wordOccurences.stream().map(i -> i + offset).collect(Collectors.toList());
            occurencesWithOffset.put(entry.getKey(), wordOccurencesWithOffset);
        }

        return occurencesWithOffset;
    }

    @Override
    public List<Gem> split(Gem gem, int nWorkers) {
        List<String> lines = gem.get("lines");
        List<String> words = gem.get("words");
        int offset = gem.get("offset");
        int partSize = gem.get("partSize");

        List<List<String>> parts = Utils.split(lines, partSize);
        List<Integer> partOffsets = Utils.split(TextUtils.computeLineOffsets(lines), partSize).stream().map(l -> l.get(0)).collect(Collectors.toList());

        return IntStream.range(0, parts.size())
                .mapToObj(i -> createGem(parts.get(i), words, offset + partOffsets.get(i), partSize, gem.getGemAccessMode()))
                .collect(Collectors.toList());
    }

    @Override
    public Map<String, List<Integer>> join(Gem gem, List<Map<String, List<Integer>>> results) {
        List<String> words = gem.get("words");

        Map<String, List<Integer>> result = words.stream().collect(Collectors.toMap(w -> w, w -> new ArrayList<Integer>()));

        for (Map<String, List<Integer>> workerResult : results) {
            for (Map.Entry<String, List<Integer>> entry : workerResult.entrySet()) {
                List<Integer> resultWordOccurences = result.get(entry.getKey());
                resultWordOccurences.addAll(entry.getValue());
            }
        }

        return result;
    }

    public static Gem createGem(List<String> lines, List<String> words, int offset, int partSize, GemAccessMode gemAccessMode) {
        Gem gem = new Gem(gemAccessMode);

        gem.put("lines", lines);
        gem.put("words", words);
        gem.put("offset", offset);
        gem.put("partSize", partSize);

        return gem;
    }
}
