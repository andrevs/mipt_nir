package com.sbt.mipt.gems.examples;

import com.sbt.mipt.gems.coordinator.ConnectionType;
import com.sbt.mipt.gems.coordinator.Coordinator;
import com.sbt.mipt.gems.coordinator.CoordinatorFactory;
import com.sbt.mipt.gems.message.Gem;
import com.sbt.mipt.gems.message.GemAccessMode;
import com.sbt.mipt.gems.task.WorkerStatelessTask;
import org.apache.commons.cli.*;

import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

/**
 * Base task runner class. Hides logic of common configuration parameters.
 *
 * @author Vasyukevich Andrey
 */
public abstract class BaseTaskRunner {
    protected final String utilityName;
    protected CommandLine commandLine;

    public BaseTaskRunner(String utilityName) {
        this.utilityName = utilityName;
    }

    public String getUtilityName() {
        return utilityName;
    }

    public <R> void runMain() throws IOException {
        initTaskParameters();

        Coordinator coordinator = createCoordinator();

        List<String> timeMeasurementLines = new ArrayList<>();
        List<String> resultLines = new ArrayList<>();

        for (int i = 0; i < getNumberOfRuns(); ++i) {
            Instant startTime = Instant.now();
            long startNanoTime = System.nanoTime();

            WorkerStatelessTask<R> workerStatelessTask = createWorkerStatelessTask();
            Gem taskGem = createTaskGem();

            R result = coordinator.submit(workerStatelessTask, taskGem);

            long finishNanoTime = System.nanoTime();
            Instant finishTime = Instant.now();

            double elapsedInMillis = (finishNanoTime - startNanoTime) / 1e6;

            if (measureTime()) {
                String formatString = "Run %d: %s - %s, elapsed %.6f millis";
                timeMeasurementLines.add(String.format(formatString, i, startTime, finishTime, elapsedInMillis));
            }

            if (writeOutputs()) {
                resultLines.addAll(taskResultToListOfStrings(result));
            }
        }

        handleTimeMeasurements(timeMeasurementLines);
        handleOutputs(resultLines);

        coordinator.close();
    }

    public void initTaskParameters() {
    }

    public abstract <R, T extends WorkerStatelessTask<R>> T createWorkerStatelessTask();

    public abstract Gem createTaskGem();

    public abstract List<String> taskResultToListOfStrings(Object result);

    public void parseCommandLineArguments(String[] arguments) {
        CommandLineParser parser = new DefaultParser();
        Options options = createOptions();

        try {
            Options helpOptions = new Options();
            helpOptions.addRequiredOption("h", "help", false, "Print help");
            parser.parse(helpOptions, arguments);
            printHelp(options);
            System.exit(0);
        } catch (ParseException e) {
        }

        try {
            commandLine = parser.parse(options, arguments);
        } catch (ParseException e) {
            printHelp(options);
            throw new RuntimeException("Failed to parse command line arguments: " + e.getMessage(), e);
        }

        if (commandLine.hasOption("help")) {
            printHelp(options);
            System.exit(0);
        }
    }

    public void printHelp(Options options) {
        HelpFormatter helpFormatter = new HelpFormatter();

        helpFormatter.printHelp(180, utilityName, null, options, null);
    }

    public Options createOptions() {
        Options options = new Options();

        options.addOption("h", "help", false, "Print help");

        options.addOption(createOption("nr", "numberOfRuns", true, false, false,
                "Number of runs.", "1"));
        options.addOption(createOption("nw", "numberOfWorkers", true, false, false,
                "Number of workers.", "1"));
        options.addOption(createOption("c", "connectionType", true, false, false,
                "Connection type. One of: LOCAL, INET.", "LOCAL"));
        options.addOption(createOption(null, "maxObjectSize", true, false, false,
                "Maximum supported object size for INET connection.", "1048576"));
        options.addOption(createOption(null, "gemAccessMode", true, false, false,
                "Gem access mode. One of: DEFAULT, READ_ONLY.", "DEFAULT"));
        options.addOption(createOption("mt", "measureTime", true, true, false,
                "Measure time of each run. Write it to file, if it was provided as argument.", "System.err"));
        options.addOption(createOption("wo", "writeOutput", true, true, false,
                "Write output of each run. Write it to file, if it was provided as argument.", "System.out"));

        return options;
    }

    public Option createOption(String opt, String longOpt, boolean hasArg, boolean optArg, boolean required, String baseDesc, String defaultValue) {
        String desc = createDescription(baseDesc, required, hasArg, optArg, defaultValue);

        return Option.builder(opt).longOpt(longOpt).hasArg(hasArg).optionalArg(optArg).required(required).desc(desc).build();
    }

    public String createDescription(String baseDesc, boolean required, boolean hasArg, boolean optArg, String defaultValue) {
        String requiredDesc = " " + (required ? "Required" : "Optional") + ".";
        String argDesc = (hasArg ? " " + (optArg ? "Optional" : "Required") + " argument." : "");
        String defaultValueDesc = " " + (defaultValue != null ? "Default value: " + defaultValue : "No default value") + ".";

        return baseDesc + requiredDesc + argDesc + defaultValueDesc;
    }

    public int getNumberOfRuns() {
        return Integer.valueOf(commandLine.getOptionValue("numberOfRuns", "1"));
    }

    public Coordinator createCoordinator() {
        int nWorkers = Integer.valueOf(commandLine.getOptionValue("numberOfWorkers", "1"));
        ConnectionType connectionType = ConnectionType.valueOf(commandLine.getOptionValue("connectionType", "LOCAL"));
        int maxObjectSize = Integer.valueOf(commandLine.getOptionValue("maxObjectSize", "1048576"));

        return CoordinatorFactory.createCoordinator(nWorkers, connectionType, maxObjectSize);
    }

    public GemAccessMode getGemAccessMode() {
        return GemAccessMode.valueOf(commandLine.getOptionValue("gemAccessMode", "DEFAULT"));
    }

    public boolean measureTime() {
        return commandLine.hasOption("measureTime");
    }

    public void handleTimeMeasurements(List<String> lines) throws IOException {
        writeLines("measureTime", lines, System.err);
    }

    public boolean writeOutputs() {
        return commandLine.hasOption("writeOutput");
    }

    public void handleOutputs(List<String> lines) throws IOException {
        writeLines("writeOutput", lines, System.out);
    }

    private void writeLines(String opt, List<String> lines, PrintStream defaultPrintStream) throws IOException {
        if (commandLine.hasOption(opt)) {
            String fileName = commandLine.getOptionValue(opt);
            PrintStream printStream = (fileName != null ? new PrintStream(Paths.get(fileName).toFile()) : defaultPrintStream);

            for (String line : lines) {
                printStream.println(line);
            }

            if (fileName != null) {
                printStream.close();
            }
        }
    }
}
