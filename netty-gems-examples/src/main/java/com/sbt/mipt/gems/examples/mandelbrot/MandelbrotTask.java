package com.sbt.mipt.gems.examples.mandelbrot;

import com.sbt.mipt.gems.examples.Utils;
import com.sbt.mipt.gems.message.Gem;
import com.sbt.mipt.gems.message.GemAccessMode;
import com.sbt.mipt.gems.task.WorkerStatelessTask;
import org.apache.commons.math3.complex.Complex;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Task for computing mandelbrot set using gems model.
 *
 * @author Vasyukevich Andrey
 */
public class MandelbrotTask extends WorkerStatelessTask<List<Complex>> {

    @Override
    public List<Complex> execute(Gem gem) {
        List<Complex> points = gem.get("points");
        int maxIter = gem.get("maxIter");

        return points.stream().filter(point -> isInMandelbrotSet(point, maxIter)).collect(Collectors.toList());
    }

    @Override
    public List<Gem> split(Gem gem, int nWorkers) {
        List<Complex> points = gem.get("points");
        int maxIter = gem.get("maxIter");
        int partSize = gem.get("partSize");

        return Utils.split(points, partSize).stream()
                .map(part -> createGem(part, maxIter, partSize, gem.getGemAccessMode()))
                .collect(Collectors.toList());
    }

    @Override
    public List<Complex> join(Gem gem, List<List<Complex>> results) {
        return results.stream().flatMap(List::stream).collect(Collectors.toList());
    }

    public static Gem createGem(List<Complex> points, int maxIter, int partSize, GemAccessMode gemAccessMode) {
        Gem gem = new Gem(gemAccessMode);

        gem.put("points", points);
        gem.put("maxIter", maxIter);
        gem.put("partSize", partSize);

        return gem;
    }

    public static boolean isInMandelbrotSet(Complex point, int maxIter) {
        Complex z = Complex.ZERO;

        for (int i = 0; i < maxIter; ++i) {
            z = z.multiply(z).add(point);

            if (z.abs() > 2) {
                return false;
            }
        }

        return true;
    }

    public static List<Complex> generateGrid(double minX, double maxX, double minY, double maxY, double cellSize) {
        List<Complex> points = new ArrayList<>();

        for (double x = minX; x < maxX; x += cellSize) {
            for (double y = minY; y < maxY; y += cellSize) {
                points.add(Complex.valueOf(x, y));
            }
        }

        return points;
    }
}
