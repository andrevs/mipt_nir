package com.sbt.mipt.gems.examples.wordcount;

import com.sbt.mipt.gems.examples.TextUtils;
import com.sbt.mipt.gems.examples.Utils;
import com.sbt.mipt.gems.message.Gem;
import com.sbt.mipt.gems.message.GemAccessMode;
import com.sbt.mipt.gems.task.WorkerStatelessTask;
import org.apache.commons.lang3.mutable.MutableInt;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Task for word count using gems model.
 *
 * @author Vasyukevich Andrey
 */
public class WordcountTask extends WorkerStatelessTask<Map<String, MutableInt>> {

    @Override
    public Map<String, MutableInt> execute(Gem gem) {
        List<String> lines = gem.get("lines");

        Map<String, MutableInt> wordcounts = new HashMap<>();

        for (String line : lines) {
            for (String word : TextUtils.splitIntoWords(line)) {
                wordcounts.compute(word, (k, v) -> {
                    if (v == null) {
                        v = new MutableInt(0);
                    }
                    v.increment();
                    return v;
                });
            }
        }

        return wordcounts;
    }

    @Override
    public List<Gem> split(Gem gem, int nWorkers) {
        List<String> lines = gem.get("lines");
        int partSize = gem.get("partSize");

        return Utils.split(lines, partSize).stream()
                .map(part -> createGem(part, partSize, gem.getGemAccessMode()))
                .collect(Collectors.toList());
    }

    @Override
    public Map<String, MutableInt> join(Gem gem, List<Map<String, MutableInt>> results) {
        Map<String, MutableInt> result = new HashMap<>();

        for (Map<String, MutableInt> workerResult : results) {
            for (Map.Entry<String, MutableInt> entry : workerResult.entrySet()) {
                result.compute(entry.getKey(), (k, v) -> {
                    if (v == null) {
                        v = new MutableInt(0);
                    }
                    v.add(entry.getValue());
                    return v;
                });
            }
        }

        return result;
    }

    public static Gem createGem(List<String> lines, int partSize, GemAccessMode gemAccessMode) {
        Gem gem = new Gem(gemAccessMode);

        gem.put("lines", lines);
        gem.put("partSize", partSize);

        return gem;
    }
}
