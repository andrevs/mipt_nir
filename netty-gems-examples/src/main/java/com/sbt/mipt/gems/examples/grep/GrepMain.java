package com.sbt.mipt.gems.examples.grep;

import com.sbt.mipt.gems.examples.BaseTaskRunner;
import com.sbt.mipt.gems.examples.TextUtils;
import com.sbt.mipt.gems.message.Gem;
import org.apache.commons.cli.Options;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Main class for computing grep using gems model.
 *
 * @author Vasyukevich Andrey
 */
public class GrepMain {
    public static void main(String[] args) throws Exception {
        BaseTaskRunner grepTaskRunner = new GrepTaskRunner();

        grepTaskRunner.parseCommandLineArguments(args);

        grepTaskRunner.runMain();
    }

    public static class GrepTaskRunner extends BaseTaskRunner {
        private List<String> lines;
        private List<String> words;
        private int partSize;

        public GrepTaskRunner() {
            super("grep");
        }

        @Override
        public Options createOptions() {
            Options options = super.createOptions();

            options.addOption(createOption(null, "textFile", true, false, true,
                    "Text file.", null));
            options.addOption(createOption(null, "wordsFile", true, false, true,
                    "Words file.", null));
            options.addOption(createOption(null, "partSize", true, false, true,
                    "Part size.", null));

            return options;
        }

        @Override
        public void initTaskParameters() {
            try {
                lines = TextUtils.readFileLines(commandLine.getOptionValue("textFile"), false);
                words = TextUtils.readFileLines(commandLine.getOptionValue("wordsFile"), false);
                partSize = Integer.valueOf(commandLine.getOptionValue("partSize"));
            } catch (IOException e) {
                throw new RuntimeException(e.getMessage(), e);
            }
        }

        @Override
        public GrepTask createWorkerStatelessTask() {
            return new GrepTask();
        }

        @Override
        public Gem createTaskGem() {
            return GrepTask.createGem(lines, words, 0, partSize, getGemAccessMode());
        }

        @Override
        public List<String> taskResultToListOfStrings(Object result) {
            Map<String, List<Integer>> occurences = (Map<String, List<Integer>>) result;
            List<String> lines = new ArrayList<>();

            for (String word : words) {
                List<Integer> wordOccurences = occurences.getOrDefault(word, Collections.emptyList());
                String wordOccurencesString = wordOccurences.stream().map(String::valueOf).collect(Collectors.joining(", "));
                lines.add(word + ": " + wordOccurencesString);
            }

            return lines;
        }
    }
}
