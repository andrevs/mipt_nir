package com.sbt.mipt.gems.examples;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Text utils.
 * Implemented simple grep in single thread.
 *
 * @author Vasyukevich Andrey
 */
public class TextUtils {
    public static List<Integer> grep(String text, String word) {
        return grep(text, Pattern.compile(word));
    }

    public static List<Integer> grep(String text, Pattern pattern) {
        List<Integer> occurences = new ArrayList<>();

        Matcher matcher = pattern.matcher(text);

        while (matcher.find()) {
            occurences.add(matcher.start());
        }

        return occurences;
    }

    public static Map<String, List<Integer>> grep(List<String> lines, List<String> words) {
        String text = lines.stream().collect(Collectors.joining("\n"));

        Map<String, List<Integer>> occurences = new HashMap<>();

        for (String word : words) {
            occurences.put(word, grep(text, word));
        }

        return occurences;
    }

    public static List<String> readFileLines(String fileName, boolean isResource) throws IOException {
        Reader fileReader;

        if (isResource) {
            ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
            fileReader = new InputStreamReader(contextClassLoader.getResourceAsStream(fileName));
        } else {
            fileReader = new FileReader(fileName);
        }

        try (BufferedReader reader = new BufferedReader(fileReader)) {
            return reader.lines().collect(Collectors.toList());
        }
    }

    public static List<Integer> computeLineOffsets(List<String> lines) {
        int n = lines.size();
        List<Integer> offsets = new ArrayList<>(n);
        offsets.add(0);
        for (int i = 1; i < n; ++i) {
            offsets.add(offsets.get(i - 1) + lines.get(i - 1).length() + 1);
        }
        return offsets;
    }

    public static List<String> splitIntoWords(String line) {
        return Arrays.asList(line.split("\\W+"));
    }
}
