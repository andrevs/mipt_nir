package com.sbt.mipt.gems.examples.sort;

import com.sbt.mipt.gems.message.Gem;
import com.sbt.mipt.gems.message.GemAccessMode;
import com.sbt.mipt.gems.task.WorkerStatelessTask;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Task for sorting using gems model.
 *
 * @author Vasyukevich Andrey
 */
public class SortTask extends WorkerStatelessTask<List<Integer>> {

    @Override
    public List<Integer> execute(Gem gem) {
        List<Integer> numbers = gem.get("numbers");
        if (GemAccessMode.READ_ONLY.equals(gem.getGemAccessMode())) {
            numbers = new ArrayList<>(numbers);
        }

        Collections.sort(numbers);

        return numbers;
    }

    @Override
    public List<Gem> split(Gem gem, int nWorkers) {
        List<Integer> numbers = gem.get("numbers");
        int maxDepth = gem.get("maxDepth");
        int minPartToSplit = gem.get("minPartToSplit");

        if (numbers.size() < 2 || maxDepth <= 0 || numbers.size() < minPartToSplit) {
            return Collections.singletonList(createGem(numbers, maxDepth - 1, minPartToSplit, gem.getGemAccessMode()));
        } else {
            int mid = numbers.size() / 2;
            List<Integer> leftHalf = new ArrayList<>(numbers.subList(0, mid));
            List<Integer> rightHalf = new ArrayList<>(numbers.subList(mid, numbers.size()));
            Gem leftHalfGem = createGem(leftHalf, maxDepth - 1, minPartToSplit, gem.getGemAccessMode());
            Gem rightHalfGem = createGem(rightHalf, maxDepth - 1, minPartToSplit, gem.getGemAccessMode());

            return Arrays.asList(leftHalfGem, rightHalfGem);
        }
    }

    @Override
    public List<Integer> join(Gem gem, List<List<Integer>> results) {
        if (results.isEmpty()) {
            return Collections.emptyList();
        } else if (results.size() == 1) {
            return results.get(0);
        } else {
            List<Integer> leftHalf = results.get(0);
            List<Integer> rightHalf = results.get(1);

            List<Integer> result = new ArrayList<>(leftHalf.size() + rightHalf.size());

            int lPos = 0, rPos = 0;

            while (lPos < leftHalf.size() || rPos < rightHalf.size()) {
                if (rPos == rightHalf.size() || (lPos < leftHalf.size() && leftHalf.get(lPos) <= rightHalf.get(rPos))) {
                    result.add(leftHalf.get(lPos++));
                } else {
                    result.add(rightHalf.get(rPos++));
                }
            }

            return result;
        }
    }

    public static Gem createGem(List<Integer> numbers, int maxDepth, int minPartToSplit, GemAccessMode gemAccessMode) {
        Gem gem = new Gem(gemAccessMode);

        gem.put("numbers", numbers);
        gem.put("maxDepth", maxDepth);
        gem.put("minPartToSplit", minPartToSplit);

        return gem;
    }
}
